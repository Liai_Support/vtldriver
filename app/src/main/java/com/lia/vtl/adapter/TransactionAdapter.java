package com.lia.vtl.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.vtl.R;
import com.lia.vtl.bo.CompletedTripBo;
import com.lia.vtl.bo.TransactionBo;
import com.lia.vtl.utility.Session;
import com.lia.vtl.view.CompletedTrip;
import com.lia.vtl.view.TransactionActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.lia.vtl.utility.VTLUtil.context;

public class TransactionAdapter  extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    ArrayList<TransactionBo> list_transaction,arrayListFiltered;
    Context ct;
    private Session session;
    private String m_Text = "";
    private TransactionActivity transactionActivity;
    public TransactionAdapter(ArrayList<TransactionBo> list_transaction, Context ct, Session session, TransactionActivity transactionActivity) {
        this.list_transaction = list_transaction;
        this.arrayListFiltered = list_transaction;
        this.ct = ct;
        this.session = session;
        this.transactionActivity=transactionActivity;
    }

    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TransactionAdapter.ViewHolder holder, int position) {
        final TransactionBo list_transactionData = list_transaction.get(position);
         holder.tripno.setText("Trip ID: "+list_transactionData.getTripId());
         holder.tripdate.setText(" Date: : "+list_transactionData.getTripMovDate());
         holder.fromaddrs.setText( list_transactionData.getTripFrom());
         holder.toaddrss.setText( list_transactionData.getTripTo());
         holder.paytype.setText( list_transactionData.getTripPayType());
        if (list_transactionData.getTripStatus().equals("unpaid")) {
            holder.paystatus.setText("UnPaid");
            holder.paystatus.setTextColor(ContextCompat.getColor(ct,R.color.red));
        }
        else if (list_transactionData.getTripStatus().equals("advpaid")){
            holder.paystatus.setText("Advance Paid");
            holder.paystatus.setTextColor(ContextCompat.getColor(ct,R.color.green));
        }
        else{
            holder.paystatus.setText("Paid");
            holder.paystatus.setTextColor(ContextCompat.getColor(ct,R.color.green));
        }
         holder.trip.setText( list_transactionData.getTripSal());
         holder.drivesal.setText( list_transactionData.getTripSal());
         holder.trippay.setText( list_transactionData.getTripPay());
         holder.bal.setText( list_transactionData.getTripBal());
         holder.tollcharge.setText( list_transactionData.getTripToll());
         holder.repaircharge.setText( list_transactionData.getTripRepair());
         holder.miscellcharge.setText( list_transactionData.getTripMiscel());
         holder.totexpense.setText( list_transactionData.getTripTotExp());
         holder.totalbal.setText( list_transactionData.getTripTotbal());
         holder.righttot.setText( list_transactionData.getTripTotbal());
         if (list_transactionData.getTripStatus().equals("unpaid")) {
             holder.rightstatus.setText("UnPaid");
             holder.rightstatus.setTextColor(ContextCompat.getColor(ct,R.color.red));
         }
         else if (list_transactionData.getTripStatus().equals("advpaid")){
             holder.rightstatus.setText("Advance Paid");
             holder.rightstatus.setTextColor(ContextCompat.getColor(ct,R.color.green));
         }
         else{
             holder.rightstatus.setText("Paid");
             holder.rightstatus.setTextColor(ContextCompat.getColor(ct,R.color.green));
         }
         holder.viewmore.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 if (holder.blinear.getVisibility()==View.VISIBLE){
                     holder.blinear.setVisibility(View.GONE);
                 }
                 else {
                     holder.blinear.setVisibility(View.VISIBLE);
                 }
             }
         });
      /*  try {
            JSONArray compTripArray= new JSONArray(list_transactionData.getTransDetails());
            for (int i = 0; i < compTripArray.length(); i++) {
                Log.d("sdfgnm",""+compTripArray);
//                                JSONObject comtripobj = branchArray.getJSONObject(i);
//                                CompletedTripBo completedTripBo = new CompletedTripBo(comtripobj.getString("data"));
//                                list_completed_trip.add(completedTripBo);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
*/
    }

    @Override
    public int getItemCount() {
        return arrayListFiltered.size();
    }

    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<TransactionBo> arrayListFilter = new ArrayList<TransactionBo>();
                if(constraint == null|| constraint.length() == 0) {
                    results.count = list_transaction.size();
                    results.values = list_transaction;
                }
                else {
                    for (TransactionBo transModel : list_transaction) {
                        if(transModel.getTripDate().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            arrayListFilter.add(transModel);
                        }
                    }
                    results.count = arrayListFilter.size();
                    results.values = arrayListFilter;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                arrayListFiltered = (ArrayList<TransactionBo>) results.values;
                notifyDataSetChanged();
                if(arrayListFiltered.size() == 0) {
                    Toast.makeText(context, "Not Found", Toast.LENGTH_LONG).show();
                }
            }
        };
        return filter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tripno,tripdate,fromaddrs,toaddrss,paytype,paystatus,trip,drivesal,trippay,bal,tollcharge, repaircharge,miscellcharge,totexpense,righttot,rightstatus,totalbal;
        private LinearLayout blinear;
        private ImageView viewmore;
        public ViewHolder(View itemView) {
            super(itemView);
            tripno=itemView.findViewById(R.id.textlabel);
            tripdate=itemView.findViewById(R.id.fadddate);
            fromaddrs=itemView.findViewById(R.id.fromaddrs);
            toaddrss=itemView.findViewById(R.id.toaddrs);
            viewmore=itemView.findViewById(R.id.transact_arraow);
            trip=itemView.findViewById(R.id.drivetrip);
            paytype=itemView.findViewById(R.id.drivepay);
            paystatus=itemView.findViewById(R.id.drivestatus);
            drivesal=itemView.findViewById(R.id.drivesal);
            trippay=itemView.findViewById(R.id.drivetrippay);
            bal=itemView.findViewById(R.id.divebal);
            tollcharge=itemView.findViewById(R.id.drivetoll);
            repaircharge=itemView.findViewById(R.id.driverepair);
            miscellcharge=itemView.findViewById(R.id.drivemiscell);
            totexpense=itemView.findViewById(R.id.driveexpend);
            totalbal=itemView.findViewById(R.id.drivetotbal);
            righttot=itemView.findViewById(R.id.driverighttottxt);
            rightstatus=itemView.findViewById(R.id.driverightstatus);
            blinear=itemView.findViewById(R.id.viewmorelinear);
        }
    }
}
