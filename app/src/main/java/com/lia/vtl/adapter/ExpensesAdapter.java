package com.lia.vtl.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.vtl.R;
import com.lia.vtl.bo.Expenses;
import com.lia.vtl.utility.Session;
import com.lia.vtl.view.OngoingTrip;

import java.util.ArrayList;

public class ExpensesAdapter extends RecyclerView.Adapter<ExpensesAdapter.ViewHolder> {
    ArrayList<Expenses> list_expenses;
    Context ct;
    private Session session;
    private String m_Text = "";
    private String selExp="";
    private OngoingTrip ongoingTrip;
    public ExpensesAdapter(ArrayList<Expenses> list_expenses, Context ct, Session session, OngoingTrip ongoingTrip) {
        this.list_expenses = list_expenses;
        this.ct = ct;
        this.session = session;
        this.ongoingTrip = ongoingTrip;
    }

    @Override
    public ExpensesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expense_layout, parent, false);
        return new ExpensesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ExpensesAdapter.ViewHolder holder, int position) {
        final Expenses listexpenseData = list_expenses.get(position);
        Log.d("wrdfg",""+listexpenseData);
        listexpenseData.getExpId();
        //Initilize with a value
        MutableLiveData<String> listen = new MutableLiveData<>();
        listen.setValue(selExp);
        listen.observe(ongoingTrip, new androidx.lifecycle.Observer<String>() {
            @Override
            public void onChanged(String s) {
                Log.d("aesrdtssdfsefsadyu",selExp);
            }
        });
            if (selExp== "TollCharge" || selExp.equals("TollCharge")) {
                holder.tollchargelinear.setVisibility(View.VISIBLE);
                holder.repairlinear.setVisibility(View.GONE);
                holder.misclilinear.setVisibility(View.GONE);
            }
            else if (selExp == "Repair" || selExp.equals("Repair")) {
                holder.tollchargelinear.setVisibility(View.GONE);
                holder.repairlinear.setVisibility(View.VISIBLE);
                holder.misclilinear.setVisibility(View.GONE);
            }
            else if (selExp == "Miscellaneous" || selExp.equals("Miscellaneous")) {
                holder.tollchargelinear.setVisibility(View.GONE);
                holder.repairlinear.setVisibility(View.GONE);
                holder.misclilinear.setVisibility(View.VISIBLE);
            }
    }

    public String selctval(String selectexpesnse) {
        selExp=selectexpesnse;
        Log.d("aesrdtyu",selExp);
        return selectexpesnse;
    }

    @Override
    public int getItemCount() {
        return list_expenses.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tripno, fromaddrs, toaddrss, fromdate, tripsal, viewmore, viewless, vname, vtype, vregno, vpermit;
        public LinearLayout tollchargelinear,repairlinear,misclilinear;
        private CardView cardlayout;
        public ViewHolder(View itemView) {
            super(itemView);
            tollchargelinear = itemView.findViewById(R.id.tollchargelinear);
            repairlinear = itemView.findViewById(R.id.repairlinear);
            misclilinear = itemView.findViewById(R.id.misclilinear);
            fromdate = itemView.findViewById(R.id.rvfadddate);
            tripsal = itemView.findViewById(R.id.rvtripsal);
            viewmore = itemView.findViewById(R.id.viewmoretxt);
            viewless = itemView.findViewById(R.id.viewlesstxt);
            vname = itemView.findViewById(R.id.rvvechnametxt);
            vtype = itemView.findViewById(R.id.rvvechtypext);
            vregno = itemView.findViewById(R.id.rvvechregnotxt);
            vpermit = itemView.findViewById(R.id.rvpermittxt);
            cardlayout = itemView.findViewById(R.id.card3);
        }
    }
}
