package com.lia.vtl.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.vtl.R;
import com.lia.vtl.bo.CompletedTripBo;
import com.lia.vtl.utility.Session;
import com.lia.vtl.view.CompletedTrip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class CompletedTripAdapter extends RecyclerView.Adapter<CompletedTripAdapter.ViewHolder> {
    List<CompletedTripBo> list_completed_trip;
    Context ct;
    private Session session;
    private String m_Text = "";
    private CompletedTrip completedTrip;

    public CompletedTripAdapter(List<CompletedTripBo> list_completed_trip, Context ct, Session session, CompletedTrip completedTrip) {
        this.list_completed_trip = list_completed_trip;
        this.ct = ct;
        this.session = session;
        this.completedTrip=completedTrip;
    }

    @Override
    public CompletedTripAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.completed_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CompletedTripAdapter.ViewHolder holder, int position) {
        final CompletedTripBo listComptripData = list_completed_trip.get(position);
        holder.tripno.setText("Trip ID: "+listComptripData.getTripId());
        holder.fromdate.setText("Date: "+listComptripData.getTripDate());
        holder.fromaddrs.setText(listComptripData.getTripFrom());
        holder.toaddrss.setText(listComptripData.getTripTo());
        holder.tripsal.setText(listComptripData.getTripSal());
        holder.vname.setText(listComptripData.getTripVechName());
        holder.vtype.setText(listComptripData.getTripVechType());
        holder.vregno.setText(listComptripData.getTripVechReg());
        holder.vpermit.setText(listComptripData.getTripVecPermit());
        if (listComptripData.getPayStatus().equals("paid")) {
            holder.paystatus.setText("Paid");
            holder.paystatus.setTextColor(ContextCompat.getColor(ct,R.color.green));
        }
        else{
            holder.paystatus.setText("UnPaid");
            holder.paystatus.setTextColor(ContextCompat.getColor(ct,R.color.red));
        }
        holder.viewmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.viewmore.setVisibility(View.GONE);
                holder.cardlayout.setVisibility(View.VISIBLE);
            }
        });
        holder.viewless.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.viewmore.setVisibility(View.VISIBLE);
                holder.cardlayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list_completed_trip.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       private TextView tripno,fromaddrs,toaddrss,fromdate,tripsal,viewmore,viewless,vname,vtype,vregno,vpermit,paystatus;
        private LinearLayout blinear;
        private CardView cardlayout;
        public ViewHolder(View itemView) {
            super(itemView);
            tripno=itemView.findViewById(R.id.rvtextlabel);
            fromaddrs=itemView.findViewById(R.id.rvfaddrestxt);
            toaddrss=itemView.findViewById(R.id.rvtoaddrstxt);
            fromdate=itemView.findViewById(R.id.rvfadddate);
            tripsal=itemView.findViewById(R.id.rvtripsal);
            viewmore=itemView.findViewById(R.id.viewmoretxt);
            viewless=itemView.findViewById(R.id.viewlesstxt);
            paystatus=itemView.findViewById(R.id.paystatus);
            vname=itemView.findViewById(R.id.rvvechnametxt);
            vtype=itemView.findViewById(R.id.rvvechtypext);
            vregno=itemView.findViewById(R.id.rvvechregnotxt);
            vpermit=itemView.findViewById(R.id.rvpermittxt);
            cardlayout=itemView.findViewById(R.id.card3);
        }
    }
}
