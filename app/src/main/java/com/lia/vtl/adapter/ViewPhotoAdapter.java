package com.lia.vtl.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lia.vtl.R;
import com.lia.vtl.bo.MultiImg;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.view.ViewPhoto;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class ViewPhotoAdapter extends RecyclerView.Adapter<ViewPhotoAdapter.ViewHolder> {
    List<MultiImg> list_multiimg;
    Context ct;
    public ViewPhotoAdapter(List<MultiImg> list_multiimg, Context ct) {
        this.list_multiimg = list_multiimg;
        this.ct = ct;
    }

    @Override
    public ViewPhotoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.multi_image_layout, parent, false);
        return new ViewPhotoAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewPhotoAdapter.ViewHolder holder, int position) {
        final MultiImg listmultiImg = list_multiimg.get(position);
        Picasso.get()
                .load(StaticInfo.imgPathStr+listmultiImg.getImgName())
                .placeholder(R.drawable.defaultimg)
                .into(holder.img);
        Log.d("image", StaticInfo.imgPathStr+listmultiImg.getImgName());
    }

    @Override
    public int getItemCount() {
        return list_multiimg.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.mimg);
        }
    }
}