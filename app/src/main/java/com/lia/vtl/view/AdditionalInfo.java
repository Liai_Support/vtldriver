package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.vtl.R;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.utility.ConvertSteam;
import com.lia.vtl.utility.HttpPostData;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AdditionalInfo extends AppCompatActivity implements View.OnClickListener{
    EditText mobno,cpemployer,rpername,refcontact;
    RadioGroup insurannce;
    RadioButton iyes,ino;
    Button cancel,draft;
    TextView next;
    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        session = new Session(this);
        mobno=(EditText)findViewById(R.id.mobno);
        cpemployer=(EditText)findViewById(R.id.cpemployer);
        rpername=(EditText)findViewById(R.id.rpername);
        refcontact=(EditText)findViewById(R.id.refcontact);
        iyes=(RadioButton) findViewById(R.id.iyes);
        ino=(RadioButton)findViewById(R.id.ino);
        mobno.setText(StaticInfo.driver.getEmerContNo());
        cpemployer.setText(StaticInfo.driver.getEmployeeType());
        rpername.setText(StaticInfo.driver.getReferPersonName());
        refcontact.setText(StaticInfo.driver.getReferContactNo());
        if (StaticInfo.driver.getInsurance() == null){
        }
        else {
            if (StaticInfo.driver.getInsurance().equals("yes")){
                iyes.setChecked(true);
            }
            else{
                ino.setChecked(true);
            }
        }
        draft=(Button)findViewById(R.id.draft);
        cancel=(Button)findViewById(R.id.cancel);
        next=(TextView)findViewById(R.id.btn_next);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
         /*
         If Status is "A" Draft button Hide
         */
        if(StaticInfo.driver.getStatus()!=null&&StaticInfo.driver.getStatus().equalsIgnoreCase("V")){
            draft.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);
        }
        else{
            draft.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }
        cancel.setOnClickListener(this);
        draft.setOnClickListener(this);
        iyes.setOnClickListener(this);
        ino.setOnClickListener(this);
        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mobno=(EditText)findViewById(R.id.mobno);
        cpemployer=(EditText)findViewById(R.id.cpemployer);
        rpername=(EditText)findViewById(R.id.rpername);
        refcontact=(EditText)findViewById(R.id.refcontact);
        iyes=(RadioButton) findViewById(R.id.iyes);
        ino=(RadioButton)findViewById(R.id.ino);
        if (view == draft) {
            String mobnoStr=mobno.getText().toString().trim();
            String cpemployerStr=cpemployer.getText().toString().trim();
            String rpernameStr=rpername.getText().toString().trim();
            String rpercontactStr=refcontact.getText().toString().trim();
            StaticInfo.driver.setEmerContNo(mobnoStr);
            StaticInfo.driver.setEmployeeType(cpemployerStr);
            StaticInfo.driver.setReferPersonName(rpernameStr);
            StaticInfo.driver.setReferContactNo(rpercontactStr);
            if(mobnoStr.equalsIgnoreCase(rpercontactStr)){
                Toast.makeText(this, "Emergency Number or Contact Number Change any one", Toast.LENGTH_SHORT).show();
                return;
            }
            new AddInfoDraftAsync().execute(StaticInfo.TEST_URL,"Post");
        }
        else if(view == iyes){
            StaticInfo.driver.setInsurance("yes");
        }
        else if(view == ino){
            StaticInfo.driver.setInsurance("No");
        }
        else if(view == cancel){
            Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
            startActivity(intent);
        }
        else if(view == next){
            String mobnoStr=mobno.getText().toString().trim();
            String cpemployerStr=cpemployer.getText().toString().trim();
            String rpernameStr=rpername.getText().toString().trim();
            String rpercontactStr=refcontact.getText().toString().trim();
            StaticInfo.driver.setStatus("V");
            StaticInfo.driver.setEmerContNo(mobnoStr);
            StaticInfo.driver.setEmployeeType(cpemployerStr);
            StaticInfo.driver.setReferPersonName(rpernameStr);
            StaticInfo.driver.setReferContactNo(rpercontactStr);
            Intent intent = new Intent(getBaseContext(), PaymentInfo.class);
            startActivity(intent);
        }
    }
    /*** Web Servcice Client  ***/
    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s",result);
                if(result.equals("success")){
                    Toast.makeText(getBaseContext(), " successful.", Toast.LENGTH_LONG).show();
                    new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                }
                else{
                    Toast.makeText(getBaseContext(), " Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncupdateTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s",result);
                if(result.indexOf("success::")!=-1){
                    Log.d("received",result);
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Driver>>() {});
                    StaticInfo.driver=  driverlist.get(0);
                    session.setusename(""+result);
                    Toast.makeText(getBaseContext(), "successful.", Toast.LENGTH_LONG).show();
                }else{
                    Log.d("session","error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class AddInfoDraftAsync extends AsyncTask<String, String, String> {
        HttpPostData objHttpPostData;
        String URL;
        String postParam = "";
        String EVENT_TAG = AddInfoDraftAsync.class.getSimpleName();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL=arg[0]+StaticInfo.DRIVER_UPDATE;
                Log.i(EVENT_TAG, "URL: " + URL);
                String res = "";
                postParam = jsonObject().toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG,"AdditionInfo Draft Post--url--"+URL+"--Params--"+postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {

                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "AdditionInfo Draft RESPONSE: " + res);
                if (res.equals("success")) {
                    new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                }
                else {
                    Toast.makeText(getBaseContext(), "Registration Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return "";
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
    public static JSONObject jsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",StaticInfo.driver.getId());
            jsonObject.put("uname",StaticInfo.driver.getUname());
            jsonObject.put("userImg", StaticInfo.driver.getUserImg());
            jsonObject.put("dob", StaticInfo.driver.getDob());
            jsonObject.put("gender", StaticInfo.driver.getGender());
            jsonObject.put("maritalStatus", StaticInfo.driver.getMaritalStatus());
            jsonObject.put("address",StaticInfo.driver.getAddress());
            jsonObject.put("city", StaticInfo.driver.getCity());
            jsonObject.put("state", StaticInfo.driver.getState());
            jsonObject.put("country", StaticInfo.driver.getCountry());
            jsonObject.put("adhar",StaticInfo.driver.getAdhar());
            jsonObject.put("aadhaarImg","aadharTestImg");
            jsonObject.put("pancard", StaticInfo.driver.getPancard());
            jsonObject.put("licenseNo",  StaticInfo.driver.getLicenseNo());
            jsonObject.put("licenceFrontImg", "licFrontTestImg");
            jsonObject.put("licenceBackImg", "licBackTestImg");
            jsonObject.put("licenseExpiryDate", StaticInfo.driver.getLicenseExpiryDate());
            jsonObject.put("batch", StaticInfo.driver.getBatch());
            jsonObject.put("batchExpiry", StaticInfo.driver.getBatchExpiry());
            jsonObject.put("drivingExperience", StaticInfo.driver.getDrivingExperience());
            jsonObject.put("vehicleHandling",StaticInfo.driver.getVehicleHandling());
            jsonObject.put("insurance",StaticInfo.driver.getInsurance());
            jsonObject.put("emerContNo", StaticInfo.driver.getEmerContNo());
            jsonObject.put("employeeType", StaticInfo.driver.getEmployeeType());
            jsonObject.put("referPersonName", StaticInfo.driver.getReferPersonName());
            jsonObject.put("referContactNo", StaticInfo.driver.getReferContactNo());
            jsonObject.put("bankAcNo",StaticInfo.driver.getBankAcNo());
            jsonObject.put("passbookImg", "passBookTestImg");
            jsonObject.put("advancePayment", StaticInfo.driver.getAdvancePayment());
            jsonObject.put("salaryCycle", StaticInfo.driver.getSalaryCycle());
            jsonObject.put("splitPayment", StaticInfo.driver.getSplitPayment());
            jsonObject.put("status",StaticInfo.driver.getStatus());
            jsonObject.put("pincode", StaticInfo.driver.getPincode());
        }
        catch (JSONException e) {
            e.getMessage();
        }
        return jsonObject;
    }
}