package com.lia.vtl.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.vtl.R;
import com.lia.vtl.bo.Article;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.startup.ThanksActivity;
import com.lia.vtl.utility.ConvertSteam;
import com.lia.vtl.utility.HttpGetData;
import com.lia.vtl.utility.HttpPostData;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.utility.VTLUtil;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

import static com.lia.vtl.view.FirstRegisterActivity.imgStrBase;

public class SecondRegisterActivity extends AppCompatActivity implements View.OnClickListener{
    EditText adhar,dlicense,expdate;
    RadioGroup gender,marital;
    RadioButton mbtn,fmbtn,obtn,marbtn,unmarbtn;
    Spinner exp,vhandle;
    Button draft,submt;
    private Session session;
    private int mYear,mcyear, mMonth, mDay, mHour, mMinute;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    private  static String adharStr,dlicenseStr,expdateStr,expStr,vhandleStr;
    private static  String adharBitmapStr, licFrontBitmapStr, licBackBitmapStr;
    private boolean adharPic,licFrontFrontPic, licBackPic;
    private ImageView adharImg,licFrontFrontImg, licBackImg;
    private ProgressBar adhprogressBar,lfprogressbar,lbprogressbar;
    public static final int CAMERA_REQUEST = 1888;
    private int[] textViews = new int[]{R.id.takeaadhar, R.id.takelicense_frd, R.id.takelicense_back};
    private static VTLUtil __UTIL = VTLUtil.getInstance();
    private File photoFile;
    String currentPhotoPath;
    public static final int CAMERA_REQUEST_CODE = 102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        session = new Session(this);
        if(__UTIL.getContext()==null){
            __UTIL.setContext(this);
        }
        adhar=(EditText)findViewById(R.id.adharno);
        adhar.setInputType(InputType.TYPE_CLASS_TEXT);
        dlicense=(EditText)findViewById(R.id.dlicenseno);
        dlicense.setInputType(InputType.TYPE_CLASS_TEXT);
        expdate=(EditText)findViewById(R.id.dexpiry);
        exp=(Spinner) findViewById(R.id.exp);
        vhandle=(Spinner) findViewById(R.id.vhandle);
        draft=(Button)findViewById(R.id.draft);
        submt=(Button)findViewById(R.id.submit);
        adharImg = findViewById(R.id.aadharpic);
        licFrontFrontImg= findViewById(R.id.license_frdpic);
        licBackImg =findViewById(R.id.license_backpic);
        adhprogressBar = (ProgressBar) findViewById(R.id.adharprogress);
        lfprogressbar = (ProgressBar) findViewById(R.id.licenseprogress);
        lbprogressbar = (ProgressBar) findViewById(R.id.licensebackprogress);
        if(StaticInfo.driver.getAadhaarImg()!=null || StaticInfo.driver.getAadhaarImg()!=""){
            adhprogressBar.setVisibility(View.VISIBLE);
            adharImg.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getAadhaarImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            adhprogressBar.setVisibility(View.GONE);
                            adharImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            adhprogressBar.setVisibility(View.GONE);
                            adharImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(adharImg);
            Log.d("asd","https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getAadhaarImg());
        }
        else{
            adharImg.setImageDrawable(getDrawable(R.drawable.defaultimg));
        }
        if(StaticInfo.driver.getLicenceFrontImg()!=null || StaticInfo.driver.getLicenceFrontImg()!=""){
            lfprogressbar.setVisibility(View.VISIBLE);
            licFrontFrontImg.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getLicenceFrontImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            lfprogressbar.setVisibility(View.GONE);
                            licFrontFrontImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            lfprogressbar.setVisibility(View.GONE);
                            licFrontFrontImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(licFrontFrontImg);
            Log.d("asd","https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getLicenceFrontImg());
        }
        else{
            licFrontFrontImg.setImageDrawable(getDrawable(R.drawable.defaultimg));
        }
        if(StaticInfo.driver.getLicenceBackImg()!=null || StaticInfo.driver.getLicenceBackImg()!="") {
            lbprogressbar.setVisibility(View.VISIBLE);
            licBackImg.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getLicenceFrontImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            lbprogressbar.setVisibility(View.GONE);
                            licBackImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            lbprogressbar.setVisibility(View.GONE);
                            licBackImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(licBackImg);
            Log.d("asd","https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getLicenceBackImg());
        }
        else{
            licBackImg.setImageDrawable(getDrawable(R.drawable.defaultimg));
        }
        //callback method to call credentialsProvider method.
        __UTIL.s3credentialsProvider();
        // callback method to call the setTransferUtility method
        __UTIL.setTransferUtility();
         /*
          Take Camera  TextView OnClick Listener Added.
         */
        for (int textViewId : textViews) {
            TextView textView = findViewById(textViewId);
            textView.setOnClickListener(this);
        }
        adhar.setText(StaticInfo.driver.getAdhar());
        dlicense.setText(StaticInfo.driver.getLicenseNo());
        expdate.setText(StaticInfo.driver.getLicenseExpiryDate());
        ArrayList<String> list=new ArrayList<String>();
       for(int i=1;i<=30;i++){
           list.add(""+i);
       }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        exp.setAdapter(adapter);
        exp.setSelection(list.indexOf(StaticInfo.driver.getDrivingExperience()));
        ArrayList<String> listvec=new ArrayList<String>();
        listvec.add("Truck");
        listvec.add("Container");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, listvec);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vhandle.setAdapter(adapter1);
        vhandle.setSelection(listvec.indexOf(StaticInfo.driver.getVehicleHandling()));
        expdate.setKeyListener(null);
        expdate.setOnClickListener(this);
        if(StaticInfo.driver.getStatus()!=null&& StaticInfo.driver.getStatus().equalsIgnoreCase("A") || StaticInfo.driver.getStatus().equalsIgnoreCase("V")){
            draft.setVisibility(View.GONE);
        }
        else{
            draft.setVisibility(View.VISIBLE);
        }
        draft.setOnClickListener(this);
        submt.setOnClickListener(this);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), FirstRegisterActivity.class);
                startActivity(intent);
            }
        });
    }
    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public void uploadFileToS3(String name, Uri contentUri){
        File storeDirectory = new File(currentPhotoPath);
        TransferObserver transferObserver = __UTIL.transferUtility.upload(
                __UTIL.bucket,     /* The bucket to upload to */
                name,    /* The key for the uploaded object */
                storeDirectory       /* The file where the data to upload exists */
        );
        __UTIL.transferObserverListener(transferObserver);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d("cpath",currentPhotoPath);
        return image;
    }

    @Override
    public void onClick(View view) {
        if (view == draft) {
            adhar=(EditText)findViewById(R.id.adharno);
            dlicense=(EditText)findViewById(R.id.dlicenseno);
            expdate=(EditText)findViewById(R.id.dexpiry);
            exp=(Spinner) findViewById(R.id.exp);
            vhandle=(Spinner) findViewById(R.id.vhandle);
            adharStr=adhar.getText().toString().trim();
            dlicenseStr=dlicense.getText().toString().trim();
             expdateStr=expdate.getText().toString().trim();
             expStr=exp.getSelectedItem().toString().trim();
             vhandleStr=vhandle.getSelectedItem().toString().trim();
            StaticInfo.driver.setLicenseNo(dlicenseStr);
            StaticInfo.driver.setAdhar(adharStr);
            StaticInfo.driver.setLicenseExpiryDate(expdateStr);
            StaticInfo.driver.setDrivingExperience(expStr);
            StaticInfo.driver.setVehicleHandling(vhandleStr);
            StaticInfo.driver.setStatus("D");
            new DriverRegisterDraftAsync().execute(StaticInfo.TEST_URL, "Post");
        }
        else if (view==submt){
            adhar=(EditText)findViewById(R.id.adharno);
            dlicense=(EditText)findViewById(R.id.dlicenseno);
            expdate=(EditText)findViewById(R.id.dexpiry);
            exp=(Spinner) findViewById(R.id.exp);
            vhandle=(Spinner) findViewById(R.id.vhandle);
            adharStr=adhar.getText().toString().trim();
            dlicenseStr=dlicense.getText().toString().trim();
            expdateStr=expdate.getText().toString().trim();
            expStr=exp.getSelectedItem().toString().trim();
            vhandleStr=vhandle.getSelectedItem().toString().trim();
            StaticInfo.driver.setLicenseNo(dlicenseStr);
            StaticInfo.driver.setAdhar(adharStr);
            StaticInfo.driver.setLicenseExpiryDate(expdateStr);
            StaticInfo.driver.setDrivingExperience(expStr);
            StaticInfo.driver.setVehicleHandling(vhandleStr);
            if(adharBitmapStr!=null){
                StaticInfo.driver.setAadhaarImg(adharBitmapStr);
            }
            if(licFrontBitmapStr!=null){
                StaticInfo.driver.setLicenceFrontImg(licFrontBitmapStr);
            }
            if(licBackBitmapStr!=null){
                StaticInfo.driver.setLicenceBackImg(licBackBitmapStr);
            }
            Log.d("dstatusd",StaticInfo.driver.getStatus());
            if (StaticInfo.driver.getStatus().equals("V")){
                StaticInfo.driver.setStatus("V");
            }
            else if (StaticInfo.driver.getStatus().equals("A")) {
                StaticInfo.driver.setStatus("A");
                Log.d("dstatusd",StaticInfo.driver.getStatus());
            }
            else
            {
                StaticInfo.driver.setStatus("P");
            }
            if (StaticInfo.driver.getId()!=0 && !StaticInfo.driver.getUname().equals("") && !StaticInfo.driver.getDob().equals("") && !StaticInfo.driver.getGender().equals("") && !StaticInfo.driver.getMaritalStatus().equals("")  && !StaticInfo.driver.getCity().equals("") && !StaticInfo.driver.getState().equals("") && !StaticInfo.driver.getCountry().equals("") && !adhar.getText().toString().equals("")  && !dlicense.getText().toString().equals("") && !exp.getSelectedItem().toString().equals("") && !vhandle.getSelectedItem().toString().equals("") ) {
                Log.d("sbmt", adharStr + "li" + dlicense + "expi" + expdateStr + "exper" + expStr + "vhandle" + vhandleStr);
                new DriverRegisterAsync().execute(StaticInfo.TEST_URL, "Post");
            }
            else{
                Toast.makeText(getBaseContext(), "All the Fields are Mandatory", Toast.LENGTH_LONG).show();
            }
        }
        else if (view==expdate){
            final Calendar c = Calendar.getInstance();
            mcyear=c.get(Calendar.YEAR);
            mYear = c.get(Calendar.YEAR)+10;
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            showDialog(0);
        }
        switch (view.getId()) {
            case R.id.takeaadhar:
                adharPic = true;
                licFrontFrontPic = false;
                licBackPic = false;
                takePicture();
                break;
            case R.id.takelicense_frd:
                adharPic = false;
                licFrontFrontPic = true;
                licBackPic = false;
                takePicture();
                break;
            case R.id.takelicense_back:
                adharPic = false;
                licFrontFrontPic = false;
                licBackPic = true;
                takePicture();
                break;
        }
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, mYear, mMonth, mDay);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            java.text.DecimalFormat nft = new java.text.DecimalFormat("#00.###");
            if(selectedYear<= mYear && selectedYear>mcyear) {
                expdate.setText(nft.format(selectedDay) + "-" + nft.format(selectedMonth + 1) + "-" + selectedYear);
            }else{
                expdate.setText("");
                Toast.makeText(getBaseContext(), "Invalid Expiry Date", Toast.LENGTH_LONG).show();
            }

        }
    };

    @SuppressLint("StaticFieldLeak")
    public class DriverRegisterAsync extends AsyncTask<String, Void, String> {
        HttpPostData objHttpPostData;
        HttpGetData objHttpGetData;
        String URL;
        String postParam = "";
        String EVENT_TAG = DriverRegisterAsync.class.getSimpleName();
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL=arg[0]+StaticInfo.DRIVER_UPDATE;
                Log.i(EVENT_TAG, "URL: " + URL);
                res = "";
                postParam = jsonObject().toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG,"Driver Register Post--url--"+URL+"--Params--"+postParam);
                }
                else {
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    Log.d(EVENT_TAG,"Driver Register Get -url--"+URL+"--Params--"+postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);

                }
                else {

                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
               Log.i(EVENT_TAG, "Register SUBMIT RESPONSE: " + res);
                if (res.equals("success")) {
                    Toast.makeText(SecondRegisterActivity.this, "successful.", Toast.LENGTH_LONG).show();
                    new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                }
                else {
                    Toast.makeText(getBaseContext(), "Registration Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return res;
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(res.equalsIgnoreCase("success")){
                Toast.makeText(SecondRegisterActivity.this, "Successfully Saved", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SecondRegisterActivity.this, ThanksActivity.class);
                startActivity(intent);
            }
        }
    }


    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncupdateTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s",result);
                if(result.indexOf("success::")!=-1){
                    Log.d("received",result);
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Driver>>() {});
                    StaticInfo.driver=  driverlist.get(0);
                    session.setusename(""+result);
                }
                else{
                    Log.d("session","error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncupdateSubmtTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s",result);
                if(result.indexOf("success::")!=-1){
                    Log.d("received",result);
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Driver>>() {});
                    StaticInfo.driver=  driverlist.get(0);
                    session.setusename(""+result);
                    new HttpAsyncArticleTask().execute(StaticInfo.getArticleUrl());
                }
                else{
                    Log.d("session","error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncArticleTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                StaticInfo.listArticle= mapper.readValue(result,new TypeReference<ArrayList<Article>>() {});
                StaticInfo.articleList=StaticInfo.listArticle.get(0);
                Intent intent = new Intent(getBaseContext(), ThanksActivity.class);
                startActivity(intent);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void takePicture() {
        //Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent takePictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.lia.vtl.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (adharPic) {
                    File f = new File(currentPhotoPath);
//                    adharImg.setImageURI(Uri.fromFile(f));
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    this.sendBroadcast(mediaScanIntent);
                    adharBitmapStr=f.getName();
                    Glide.with(this)
                            .load(Uri.fromFile(f))
                            .into(adharImg);
                    uploadFileToS3(f.getName(), contentUri);
                    StaticInfo.driver.setAadhaarImg(adharBitmapStr);

                }
                else if (licFrontFrontPic) {
                    File f = new File(currentPhotoPath);
//                    licFrontFrontImg.setImageURI(Uri.fromFile(f));
                    Glide.with(this)
                            .load(Uri.fromFile(f))
                            .into(licFrontFrontImg);
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));

                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    this.sendBroadcast(mediaScanIntent);
                    licFrontBitmapStr=f.getName();
                    uploadFileToS3(f.getName(), contentUri);
                    StaticInfo.driver.setAadhaarImg(licFrontBitmapStr);
                }
                else if(licBackPic){
                    File f = new File(currentPhotoPath);
//                    licBackImg.setImageURI(Uri.fromFile(f));
                    Glide.with(this)
                            .load(Uri.fromFile(f))
                            .into(licBackImg);
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    this.sendBroadcast(mediaScanIntent);
                    licBackBitmapStr=f.getName();
                    uploadFileToS3(f.getName(), contentUri);
                    StaticInfo.driver.setAadhaarImg(licBackBitmapStr);
                }
            }
        }
    }
    /*
    Aadhar Card Validation
    */
    public static boolean isAadhaarValid(String cardNumber) {
        Matcher matcher = VTLUtil.aadhaarPattern.matcher(cardNumber);
        return matcher.find();
    }
    /*
     License Validation
     */
    public static boolean isLicenseValid(String cardNumber) {
        Matcher matcher = VTLUtil.licencePattern.matcher(cardNumber);
        return matcher.find();
    }

    @SuppressLint("StaticFieldLeak")
    public class DriverRegisterDraftAsync extends AsyncTask<String, Void, String> {
        HttpPostData objHttpPostData;
        HttpGetData objHttpGetData;
        String URL;
        String postParam = "";
        String EVENT_TAG = DriverRegisterAsync.class.getSimpleName();
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL=arg[0]+StaticInfo.DRIVER_UPDATE;
                Log.i(EVENT_TAG, "URL: " + URL);
                res = "";
                postParam = jsonObject().toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG,"Driver Register Draft Post--url--"+URL+"--Params--"+postParam);
                }
                else {
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    Log.d(EVENT_TAG,"Driver Register Draft Get -url--"+URL+"--Params--"+postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {

                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "Register Draft RESPONSE: " + res);
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return res;
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s.equalsIgnoreCase("success")){
                Log.d("wesrdtf","fg");
                Toast.makeText(SecondRegisterActivity.this, "Drafted", Toast.LENGTH_SHORT).show();
                new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
            }
        }
    }

    public static JSONObject jsonObject(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",StaticInfo.driver.getId());
            jsonObject.put("uname",StaticInfo.driver.getUname());
            jsonObject.put("userImg", StaticInfo.driver.getUserImg());
            jsonObject.put("dob", StaticInfo.driver.getDob());
            jsonObject.put("gender", StaticInfo.driver.getGender());
            jsonObject.put("maritalStatus", StaticInfo.driver.getMaritalStatus());
            jsonObject.put("address",StaticInfo.driver.getAddress());
            jsonObject.put("city", StaticInfo.driver.getCity());
            jsonObject.put("state", StaticInfo.driver.getState());
            jsonObject.put("country", StaticInfo.driver.getCountry());
            if (imgStrBase!=""){
                jsonObject.put("userImgBase", imgStrBase);
            }
            jsonObject.put("adhar",StaticInfo.driver.getAdhar());
            jsonObject.put("aadhaarImg",StaticInfo.driver.getAadhaarImg());
            jsonObject.put("pancard", StaticInfo.driver.getPancard());
            jsonObject.put("licenseNo",  StaticInfo.driver.getLicenseNo());
            jsonObject.put("licenceFrontImg", StaticInfo.driver.getLicenceFrontImg());
            jsonObject.put("licenceBackImg", StaticInfo.driver.getLicenceBackImg());
            jsonObject.put("licenseExpiryDate", StaticInfo.driver.getLicenseExpiryDate());
            jsonObject.put("batch", StaticInfo.driver.getBatch());
            jsonObject.put("batchExpiry", StaticInfo.driver.getBatchExpiry());
            jsonObject.put("drivingExperience", StaticInfo.driver.getDrivingExperience());
            jsonObject.put("vehicleHandling",StaticInfo.driver.getVehicleHandling());
            jsonObject.put("insurance",StaticInfo.driver.getInsurance());
            jsonObject.put("emerContNo", StaticInfo.driver.getEmerContNo());
            jsonObject.put("employeeType", StaticInfo.driver.getEmployeeType());
            jsonObject.put("referPersonName", StaticInfo.driver.getReferPersonName());
            jsonObject.put("referContactNo", StaticInfo.driver.getReferContactNo());
            jsonObject.put("bankAcNo",StaticInfo.driver.getBankAcNo());
            jsonObject.put("passbookImg", "passBookTestImg");
            jsonObject.put("advancePayment", StaticInfo.driver.getAdvancePayment());
            jsonObject.put("salaryCycle", StaticInfo.driver.getSalaryCycle());
            jsonObject.put("splitPayment", StaticInfo.driver.getSplitPayment());
            jsonObject.put("status",StaticInfo.driver.getStatus());
            jsonObject.put("pincode", StaticInfo.driver.getPincode());

        }
        catch (JSONException e) {
            e.getMessage();
        }
        return jsonObject;
    }
}