package com.lia.vtl.view;


import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {
	public static boolean email(String email){
		 String regex = "^(.+)@(.+)$";
	      Pattern pattern = Pattern.compile(regex);
	      Matcher matcher = pattern.matcher((CharSequence) email);
	      return matcher.matches();
	}
	
	public static String decimal2(String doub){
		BigDecimal a = new BigDecimal(doub);
		a = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return(a.toString());
	}
	public static boolean mobile(String val){
		try{
			Long.parseLong(val);
			if(val.length()==10){
				return true;
			}
			else{
				return false;
			}
		}
		catch (Exception e){
			return false;
		}
	}
	public static void main(String args[]){
		System.out.println(Validation.decimal2("23.43543543"));
		System.out.println(Validation.email("vm@g.com"));
	}
}
