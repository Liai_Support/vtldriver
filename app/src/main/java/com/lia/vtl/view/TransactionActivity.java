package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.lia.vtl.R;
import com.lia.vtl.adapter.CompletedTripAdapter;
import com.lia.vtl.adapter.TransactionAdapter;
import com.lia.vtl.bo.CompletedTripBo;
import com.lia.vtl.bo.TransactionBo;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Locale;

public class TransactionActivity extends AppCompatActivity implements View.OnClickListener {
    private ArrayList<TransactionBo> list_transaction;
    private TransactionAdapter transactionAdapter;
    private GridLayoutManager brgm;
    private RecyclerView transrcv;
    private Session session;
    private Button mnthbtn;
    private  Month month;
    private TextView totearn,totpaid,totalbal,monthtot,monthpaid,monthbal;
    private static final String Url = "driverTxnDet";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        session = new Session(this);
        JSONObject obj=new JSONObject();
        try {
            obj.put("driverId", StaticInfo.userId);
            getInitialValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mnthbtn=findViewById(R.id.month_picker);
        totearn=findViewById(R.id.totearning);
        totpaid=findViewById(R.id.totpaid);
        totalbal=findViewById(R.id.totbalance);
        monthtot=findViewById(R.id.mtotal);
        monthpaid=findViewById(R.id.mpaid);
        monthbal=findViewById(R.id.mbalance);
        Log.d("TOken ",""+ FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
        Calendar today = Calendar.getInstance();
        int mnth=today.get(Calendar.MONTH)+1;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            month = Month.of(mnth);
        }
        mnthbtn.setText(today.get(Calendar.YEAR)+","+ month);
        transrcv=findViewById(R.id.trans_rc);
        brgm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL, false);
        transrcv.setLayoutManager(brgm);
        mnthbtn.setOnClickListener(this);
    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject tobj = new JSONObject(response);
                        if(tobj.getString("error")=="true"){
                        }
                        else{
                            JSONObject dataobj=tobj.getJSONObject("data");
                            JSONObject oadataobj=dataobj.getJSONObject("overAll");
                            JSONObject cmdataobj=dataobj.getJSONObject("currentmonthData");
                            totearn.setText(oadataobj.getString("totalEarning"));
                            totpaid.setText(oadataobj.getString("totalPaid"));
                            totalbal.setText(oadataobj.getString("totalBalance"));
                            monthtot.setText(cmdataobj.getString("totalEarning"));
                            monthpaid.setText(cmdataobj.getString("totalPaid"));
                            monthbal.setText(cmdataobj.getString("totalBalance"));
                            list_transaction=new ArrayList<>();
                            JSONArray transArray=dataobj.getJSONArray("tripDetails");
                            for (int i = 0; i < transArray.length(); i++) {
                                JSONObject obj = transArray.getJSONObject(i);
                                    TransactionBo transactionBo = new TransactionBo(obj.getInt("tripId"),obj.getString("mMonth")+","+obj.getString("mYear"),obj.getString("movementDate"),obj.getString("tFromcity"),
                                            obj.getString("tTocity"),obj.getString("pType"),obj.getString("driverSalary"),obj.getString("pStatus"),
                                            obj.getString("driverSalary"),obj.getString("tripPayment"),obj.getString("balance"),obj.getString("tollCharges")
                                    ,obj.getString("repairMaintenance"),obj.getString("Miscellaneous"),obj.getString("totalExp"),obj.getString("totalBalane"));
                                    list_transaction.add(transactionBo);

                            }
                            setupTransData(list_transaction);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupTransData(ArrayList<TransactionBo> list_transaction) {
        transactionAdapter=new TransactionAdapter(list_transaction,this,session,this);
        transrcv.setAdapter(transactionAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view==mnthbtn){
            final Calendar today = Calendar.getInstance();
            MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(TransactionActivity.this, new MonthPickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(int selectedMonth, int selectedYear) {
                    Log.d("TAG", "selectedMonth : " + selectedMonth + " selectedYear : " + selectedYear);
                    int mnth=selectedMonth+1;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        month = Month.of(mnth);
                    }
                    mnthbtn.setText(selectedYear+","+ month);
                    transactionAdapter.getFilter().filter(selectedMonth+1+","+selectedYear);
                }
            }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

            builder.setActivatedMonth(today.get(Calendar.MONTH))
                    .setMinYear(today.get(Calendar.YEAR)-1)
                    .setActivatedYear(today.get(Calendar.YEAR))
                    .setMaxYear(today.get(Calendar.YEAR))
                    .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                        @Override
                        public void onMonthChanged(int selectedMonth) {
                            Log.d("TAG", "Selected month : " + selectedMonth);
                        }
                    })
                    .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                        @Override
                        public void onYearChanged(int selectedYear) {
                            Log.d("TAG", "Selected year : " + selectedYear);
                        }
                    })
                    .build()
                    .show();
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(TransactionActivity.this, TripHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}