package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.vtl.R;
import com.lia.vtl.adapter.CompletedTripAdapter;
import com.lia.vtl.bo.CompletedTripBo;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CompletedTrip extends Fragment  {
    private static final String Url = "completedTrip";
    private static final String statusUrl = "updatestatus";
    int dummyColor;
    private Session session;
    private  TripActivity tripActivity;
    private List<CompletedTripBo> list_completed_trip;
    private CompletedTripAdapter completedTripAdapter;
    private GridLayoutManager brgm;
    private RecyclerView comtriprv;
    public CompletedTrip() {
    }
    @SuppressLint("ValidFragment")
    public CompletedTrip(int color) {
        this.dummyColor = color;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_completed_trip, container, false);
        comtriprv=view.findViewById(R.id.comptrip_rc);
        brgm=new GridLayoutManager(getActivity().getBaseContext(),1,GridLayoutManager.VERTICAL, false);
        comtriprv.setLayoutManager(brgm);
        JSONObject obj=new JSONObject();
        try {
            obj.put("driverId", StaticInfo.userId);
            getInitialValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    public String convertStandardJSONString(String data_json){
        data_json = data_json.replace("\"[", "[");
        data_json = data_json.replace("]\"", "]");
        return data_json;
    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        String conresponse=convertStandardJSONString(response);
                        JSONObject obj = new JSONObject(conresponse);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            list_completed_trip=new ArrayList<>();
                            JSONArray compArray=obj.getJSONArray("data");
                            for (int i = 0; i < compArray.length(); i++) {
                                JSONObject comtripobj = compArray.getJSONObject(i);
                                CompletedTripBo completedTripBo = new CompletedTripBo(comtripobj.getInt("tripId"),comtripobj.getString("movementDate"),comtripobj.getString("tFrom"),
                                        comtripobj.getString("tTo"),comtripobj.getString("tripSalary"),comtripobj.getString("transporterName"),comtripobj.getString("vType")
                                ,comtripobj.getString("regNo"),comtripobj.getString("pertmitType"),comtripobj.getString("pStatus"));
                                list_completed_trip.add(completedTripBo);
                            }
                            setupCOrderData(list_completed_trip);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupCOrderData(List<CompletedTripBo> list_completed_trip) {
        completedTripAdapter=new CompletedTripAdapter(list_completed_trip,getActivity().getBaseContext(),session,this);
        comtriprv.setAdapter(completedTripAdapter);
    }
}

