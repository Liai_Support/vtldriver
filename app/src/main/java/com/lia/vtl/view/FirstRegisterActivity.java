package com.lia.vtl.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.vtl.R;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.utility.ConvertSteam;
import com.lia.vtl.utility.CustomMethods;
import com.lia.vtl.utility.HttpGetData;
import com.lia.vtl.utility.HttpPostData;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.utility.VTLUtil;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FirstRegisterActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    static final int REQUEST_IMAGE_CAPTURE = 1;
    TextView profpicbtn,btn_next;
    ImageView profpic;
    EditText firstname,dob,mobno,pincode;
    RadioGroup gender,marital;
    RadioButton  mbtn,fmbtn,obtn,marbtn,unmarbtn;
    Spinner state,district,country;
    Button cancel,draft;
    private Session session;
    public static String imgStrBase="";
    private static String nameStr,dobStr,mobileStr,countrystr,pincodeStr,stateStr,districtStr;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    private int mYear, mMonth, mDay;
    private VTLUtil __UTIL = VTLUtil.getInstance();
    LinearLayout linearLayout1;
    PopupWindow popupWindow;
    private static String imgBaseStr;
    private String status;
    private File photoFile;
    private int[] spinners = new int[]{ R.id.state,R.id.district  };
    private String selectedState, selectedDistrict;
    public static ArrayList<String> stateList = new ArrayList<String>();
    public static ArrayList<String> districtList = new ArrayList<String>();
    private String stateDistrictURL="https://vtlpl.com/new_app/service/tech.php?action=onestate";
    String currentPhotoPath;
    public static final int CAMERA_REQUEST_CODE = 102;
    List<String> listing;
    private int percentage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_register);
        session = new Session(this);
        if(__UTIL.getContext()==null){
            __UTIL.setContext(this);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        profpic=(ImageView)findViewById(R.id.profpic);
        profpicbtn=(TextView)findViewById(R.id.takeprofpic);
        firstname=(EditText)findViewById(R.id.uname);
        dob=(EditText)findViewById(R.id.dob);
        mobno=(EditText)findViewById(R.id.mobno);
        country=(Spinner)findViewById(R.id.country);
        pincode=(EditText)findViewById(R.id.pincode);
        marital=(RadioGroup) findViewById(R.id.marital);
        gender=(RadioGroup)findViewById(R.id.gender);
        state=(Spinner) findViewById(R.id.state);
        district=(Spinner)findViewById(R.id.district);
        cancel=(Button) findViewById(R.id.cancel);
        draft=(Button)findViewById(R.id.draft);
        mbtn=(RadioButton)findViewById(R.id.mbtn);
        fmbtn=(RadioButton)findViewById(R.id.fmbtn);
        obtn=(RadioButton)findViewById(R.id.obtn);
        marbtn=(RadioButton)findViewById(R.id.marbtn);
        unmarbtn=(RadioButton)findViewById(R.id.unmarbtn);
        btn_next=(TextView) findViewById(R.id.btn_next);
        mobno.setText(StaticInfo.driver.getContactNo());
        firstname.setText(StaticInfo.driver.getUname());
        dob.setText(StaticInfo.driver.getDob());
        pincode.setText(StaticInfo.driver.getPincode());
        getState();
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        if(StaticInfo.driver.getUserImg()!=null || StaticInfo.driver.getUserImg()!=""){
            progressBar.setVisibility(View.VISIBLE);
            profpic.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getUserImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            profpic.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            profpic.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(profpic);
            Log.d("asd","https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getUserImg());
        }
        else{
            profpic.setVisibility(View.VISIBLE);
            profpic.setImageDrawable(getDrawable(R.drawable.defaultimg));
        }
        /*
         If Status is "A" Draft button Hide
         */
        if(StaticInfo.driver.getStatus()!=null&& StaticInfo.driver.getStatus().equalsIgnoreCase("A") || StaticInfo.driver.getStatus().equalsIgnoreCase("V")){
            draft.setVisibility(View.GONE);
        }
        else{
            draft.setVisibility(View.VISIBLE);
        }
        String[] arraySpinner2 = new String[] {"India"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, arraySpinner2);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country.setAdapter(adapter2);
        country.setSelection(findIndex(arraySpinner2,"India"));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, stateList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        state.setAdapter(adapter);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, districtList);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        district.setAdapter(adapter1);
        if (StaticInfo.driver.getCity() !=null ) {
            state.setSelection(stateList.indexOf(StaticInfo.driver.getState()));
            Log.d("asdfgzdczsdxcstete", "" + stateList.indexOf(StaticInfo.driver.getState()));
            getDist(StaticInfo.driver.getState());
        }
        else {
            state.setSelection(0);
        }
        // callback method to call credentialsProvider method.
        __UTIL.s3credentialsProvider();
        // callback method to call the setTransferUtility method
        __UTIL.setTransferUtility();
        dob.setKeyListener(null);
        dob.setOnClickListener(this);
        profpicbtn.setOnClickListener(this);
        draft.setOnClickListener(this);
        cancel.setOnClickListener(this);
        mbtn.setOnClickListener(this);
        fmbtn.setOnClickListener(this);
        obtn.setOnClickListener(this);
        marbtn.setOnClickListener(this);
        unmarbtn.setOnClickListener(this);
        btn_next.setOnClickListener(this);
        if (StaticInfo.driver.getGender() ==null ) {
        }
        else{
            if (StaticInfo.driver.getGender().equals("Male")) {
                mbtn.setChecked(true);
            }
            else if (StaticInfo.driver.getGender().equals("Female")) {
                fmbtn.setChecked(true);
            }
            else if (StaticInfo.driver.getGender().equals("Others")) {
                obtn.setChecked(true);
            }
        }
        if (StaticInfo.driver.getMaritalStatus() ==null ) {
        }
        else{
            if (StaticInfo.driver.getMaritalStatus().equals("Married")) {
                marbtn.setChecked(true);
            }
            else if (StaticInfo.driver.getMaritalStatus().equals("UnMarried")) {
                unmarbtn.setChecked(true);
            }
        }
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });
        for(int spinner_id : spinners){
            Spinner spinner = findViewById(spinner_id);
            spinner.setOnItemSelectedListener(this);
        }
    }

  public void uploadFileToS3(String name, Uri contentUri){
      File storeDirectory = new File(currentPhotoPath);
      TransferObserver transferObserver = __UTIL.transferUtility.upload(
              __UTIL.bucket,     /* The bucket to upload to */
              name,    /* The key for the uploaded object */
              storeDirectory       /* The file where the data to upload exists */
      );
      __UTIL.transferObserverListener(transferObserver);
    }

    private void getState() {
        stateList.clear();
       try {
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray stateNameArry = jsonObject.getJSONArray("stateList");
            for (int a = 0; a < stateNameArry.length(); a++) {
                JSONObject jo_inside = stateNameArry.getJSONObject(a);
                stateList.add(jo_inside.getString("stateName"));
            }
        }
       catch (JSONException e) {
            e.printStackTrace();
        }
        final ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, stateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        state.setAdapter(stateAdapter);
    }

    private void getDist(String selectedState) {
        try {
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray stateNameArry = jsonObject.getJSONArray("stateList");
            for (int a = 0; a < stateNameArry.length(); a++) {
                JSONObject jo_inside = stateNameArry.getJSONObject(a);
                String stateName = jo_inside.getString("stateName");
                if (stateName.contentEquals(selectedState)) {
                    JSONArray jsonDistArray = jo_inside.getJSONArray("districtList");
                    for (int b = 0; b < jsonDistArray.length(); b++) {
                        String getDist = jsonDistArray.getJSONObject(b).getString("districtName");
                        districtList.add(getDist);
                    }
                }
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        final ArrayAdapter<String> distAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item, districtList);
        distAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        district.setAdapter(distAdapter);
        if (StaticInfo.driver.getCity()!=null ){
            Log.d("getcity",StaticInfo.driver.getCity());
            Log.d("asdfgdist", "" + districtList.size());
            district.setSelection(districtList.indexOf(StaticInfo.driver.getCity()));
            Log.d("asdfgzdczsdxcdist", "" + districtList.indexOf(StaticInfo.driver.getCity()));
            String as = String.valueOf(district.getItemAtPosition(districtList.indexOf(StaticInfo.driver.getCity())));
            StaticInfo.driver.setCity(StaticInfo.driver.getCity());
            Log.d("asdfgzdczsdxcdist", "" + as);
        }
        else{
            district.setSelection(0);
            StaticInfo.driver.setCity(selectedDistrict);
        }
    }

    public static int findIndex(String arr[], String t)
    {
        if (arr == null) {
            return -1;
        }
        int len = arr.length;
        int i = 0;
        while (i < len) {
            if (arr[i] == t) {
                return i;
            }
            else {
                i = i + 1;
            }
        }
        return -1;
    }

    private File createImageFile() throws IOException {
        Log.d("create image file ","profile pic clicked");
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d("cpath",currentPhotoPath);
        return image;
    }

    @Override
    public void onClick(View view) {
        if (view == profpicbtn) {
            Log.d("clicked ","profile pic clicked");
//            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Intent takePictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                    Log.d("clickedsa ","profile pic clicked");
                } catch (IOException ex) {
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.lia.vtl.android.fileprovider",
                            photoFile);
                    Log.d("clickedsasdfsdf ","profile pic clicked");
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
                }
            }
           /* Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }*/
        }
        else if (view == dob) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR)- CustomMethods.MAX_AGE;
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            showDialog(0);

        }
        else if (view == draft) {
//            EditText firstname,dob,mobno,country,pincode;
//            RadioGroup gender,marital;
//            Spinner state,district;
            // Get Current Date
            nameStr=firstname.getText().toString().trim();
            dobStr=dob.getText().toString().trim();
            mobileStr=mobno.getText().toString().trim();
            countrystr=country.getSelectedItem().toString().trim();
            pincodeStr=pincode.getText().toString().trim();
            stateStr=state.getSelectedItem().toString().trim();
            districtStr=district.getSelectedItem().toString().trim();
            StaticInfo.driver.setUname(nameStr);
            //new HttpAsyncTask().execute(StaticInfo.getUpdateRegisterUrl(StaticInfo.driver.getId(),nameStr,StaticInfo.driver.getUserImg(), dobStr,StaticInfo.driver.getGender(),StaticInfo.driver.getMaritalStatus(),StaticInfo.driver.getAddress(),districtStr,stateStr,countrystr,StaticInfo.driver.getAdhar(),StaticInfo.driver.getAadhaarImg(),StaticInfo.driver.getPancard(),StaticInfo.driver.getLicenseNo(),StaticInfo.driver.getLicenceFrontImg(),StaticInfo.driver.getLicenceBackImg(),StaticInfo.driver.getLicenseExpiryDate(),StaticInfo.driver.getBatch(),StaticInfo.driver.getBatchExpiry(),StaticInfo.driver.getDrivingExperience(),StaticInfo.driver.getVehicleHandling(),StaticInfo.driver.getEmerContNo(),StaticInfo.driver.getInsurance(),StaticInfo.driver.getEmployeeType(),StaticInfo.driver.getReferPersonName(),StaticInfo.driver.getReferContactNo(),StaticInfo.driver.getBankAcNo(),StaticInfo.driver.getPassbookImg(),StaticInfo.driver.getAdvancePayment(),StaticInfo.driver.getSalaryCycle(),StaticInfo.driver.getSplitPayment(),"D",pincodeStr));
            new RegisterFirstPageDraftAsync().execute(StaticInfo.TEST_URL, "Post");
        }
        else if (view == btn_next) {
            String nameStr=firstname.getText().toString().trim();
            String dobStr=dob.getText().toString().trim();
            String mobileStr=mobno.getText().toString().trim();
            String countrystr=country.getSelectedItem().toString().trim();
            String pincodeStr=pincode.getText().toString().trim();
            String stateStr=state.getSelectedItem().toString().trim();
            String districtStr=district.getSelectedItem().toString().trim();
            StaticInfo.driver.setUname(nameStr);
            StaticInfo.driver.setDob(dobStr);
            StaticInfo.driver.setCountry(countrystr);
            StaticInfo.driver.setPincode(pincodeStr);
            StaticInfo.driver.setState(stateStr);
            StaticInfo.driver.setCity(districtStr);
            StaticInfo.driver.setContactNo(mobileStr);
            if(imgBaseStr!=null){
                StaticInfo.driver.setUserImg(imgBaseStr);
            }
            Intent intent = new Intent(FirstRegisterActivity.this, SecondRegisterActivity.class);
            startActivity(intent);
            //new HttpAsyncTask().execute(StaticInfo.getUpdateRegisterUrl(StaticInfo.driver.getId(),nameStr,StaticInfo.driver.getUserImg(), dobStr,StaticInfo.driver.getGender(),StaticInfo.driver.getMaritalStatus(),StaticInfo.driver.getAddress(),districtStr,stateStr,countrystr,StaticInfo.driver.getAdhar(),StaticInfo.driver.getAadhaarImg(),StaticInfo.driver.getPancard(),StaticInfo.driver.getLicenseNo(),StaticInfo.driver.getLicenceFrontImg(),StaticInfo.driver.getLicenceBackImg(),StaticInfo.driver.getLicenseExpiryDate(),StaticInfo.driver.getBatch(),StaticInfo.driver.getBatchExpiry(),StaticInfo.driver.getDrivingExperience(),StaticInfo.driver.getVehicleHandling(),StaticInfo.driver.getEmerContNo(),StaticInfo.driver.getInsurance(),StaticInfo.driver.getEmployeeType(),StaticInfo.driver.getReferPersonName(),StaticInfo.driver.getReferContactNo(),StaticInfo.driver.getBankAcNo(),StaticInfo.driver.getPassbookImg(),StaticInfo.driver.getAdvancePayment(),StaticInfo.driver.getSalaryCycle(),StaticInfo.driver.getSplitPayment(),"D",pincodeStr));
        }
        else if (view == mbtn) {
            StaticInfo.driver.setGender("Male");        }
        else if (view == fmbtn) {
            StaticInfo.driver.setGender("Female");        }
        else if (view == obtn) {
            StaticInfo.driver.setGender("Others");        }
        else if (view == marbtn) {
            StaticInfo.driver.setMaritalStatus("Married");
        Log.d("marital",StaticInfo.driver.getMaritalStatus());
        }
        else if (view == unmarbtn) {
            StaticInfo.driver.setMaritalStatus("UnMarried");
            Log.d("marital",StaticInfo.driver.getMaritalStatus());
        }
        else if (view == cancel) {
            Intent intent = new Intent(FirstRegisterActivity.this, DashboardActivity.class);
            startActivity(intent);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("sdfgh",""+requestCode);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(currentPhotoPath);
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                imgBaseStr=f.getName();
                Glide.with(this)
                        .load(Uri.fromFile(f))
                        .into(profpic);
//                profpic.setImageURI(Uri.fromFile(f));
             uploadFileToS3(f.getName(), contentUri);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(FirstRegisterActivity.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        return new DatePickerDialog(this, datePickerListener, mYear, mMonth, mDay);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            java.text.DecimalFormat nft = new java.text.DecimalFormat("#00.###");
            if(selectedYear<= mYear) {
                dob.setText(nft.format(selectedDay) + "-" + nft.format(selectedMonth + 1) + "-" + selectedYear);
            }
            else{
                dob.setText("");
                Toast.makeText(getBaseContext(), " DOB is invalid", Toast.LENGTH_LONG).show();
            }
        }
    };
    /*** Web Servcice Client  ***/
    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()){
            case R.id.state:
                selectedState = parent.getItemAtPosition(position).toString();
                districtList.clear();
                getDist(selectedState);
                StaticInfo.driver.setState(selectedState);
                break;
            case R.id.district:
                selectedDistrict = parent.getItemAtPosition(position).toString();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s",result);
                if(result.equals("success")){
                    Log.d("res",result);
                     Toast.makeText(getBaseContext(), "Draft Saved Successfully", Toast.LENGTH_LONG).show();
                    new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                    Log.d("url",StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                }
                else{
                    Toast.makeText(getBaseContext(), "Registration Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class HttpAsyncupdateTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                if(result.equals("0")){
                    Toast.makeText(getBaseContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
                }
                else if(result.indexOf("success::")!=-1){
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Driver>>() {});
                    StaticInfo.driver=  driverlist.get(0);
                    session.setusename(""+result);
                }
                else{
                    Log.d("session","error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    public class RegisterFirstPageDraftAsync extends AsyncTask<String, String, String> {
        HttpPostData objHttpPostData;
        HttpGetData objHttpGetData;
        String URL;
        String postParam = "";
        String EVENT_TAG = RegisterFirstPageDraftAsync.class.getSimpleName();
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL=arg[0]+StaticInfo.DRIVER_UPDATE;
                Log.i(EVENT_TAG, "URL: " + URL);
                 res = "";
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id",StaticInfo.driver.getId());
                    jsonObject.put("uname",nameStr);
                    if (imgStrBase!=""){
                        jsonObject.put("userImgBase", imgStrBase);
                    }
                    jsonObject.put("userImg", StaticInfo.driver.getUserImg());
                    jsonObject.put("dob", dobStr);
                    jsonObject.put("gender", StaticInfo.driver.getGender());
                    jsonObject.put("maritalStatus", StaticInfo.driver.getMaritalStatus());
                    jsonObject.put("status","D");
                    jsonObject.put("contactNumber", mobileStr);
                    jsonObject.put("country", countrystr);
                    jsonObject.put("state", stateStr);
                    jsonObject.put("city",districtStr);
                    jsonObject.put("pincode", pincodeStr);
                    jsonObject.put("adhar",StaticInfo.driver.getAdhar());
                    jsonObject.put("aadhaarImg",StaticInfo.driver.getAadhaarImg());
                    jsonObject.put("pancard", StaticInfo.driver.getPancard());
                    jsonObject.put("licenseNo",  StaticInfo.driver.getLicenseNo());
                    jsonObject.put("licenceFrontImg", StaticInfo.driver.getLicenceFrontImg());
                    jsonObject.put("licenceBackImg", StaticInfo.driver.getLicenceBackImg());
                    jsonObject.put("licenseExpiryDate", StaticInfo.driver.getLicenseExpiryDate());
                    jsonObject.put("batch", StaticInfo.driver.getBatch());
                    jsonObject.put("batchExpiry", StaticInfo.driver.getBatchExpiry());
                    jsonObject.put("drivingExperience", StaticInfo.driver.getDrivingExperience());
                    jsonObject.put("vehicleHandling",StaticInfo.driver.getVehicleHandling());
                    jsonObject.put("insurance",StaticInfo.driver.getInsurance());
                    jsonObject.put("emerContNo", StaticInfo.driver.getEmerContNo());
                    jsonObject.put("employeeType", StaticInfo.driver.getEmployeeType());
                    jsonObject.put("referPersonName", StaticInfo.driver.getReferPersonName());
                    jsonObject.put("referContactNo", StaticInfo.driver.getReferContactNo());
                    jsonObject.put("bankAcNo",StaticInfo.driver.getBankAcNo());
                    jsonObject.put("passbookImg", StaticInfo.driver.getPassbookImg());
                    jsonObject.put("advancePayment", StaticInfo.driver.getAdvancePayment());
                    jsonObject.put("salaryCycle", StaticInfo.driver.getSalaryCycle());
                    jsonObject.put("splitPayment", StaticInfo.driver.getSplitPayment());
                    jsonObject.put("status",StaticInfo.driver.getStatus());
                    Log.d("sdfghj",nameStr);
                }
                catch (JSONException e) {
                    e.getMessage();
                }
                postParam = jsonObject.toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG,"Driver Register Draft Post--url--"+URL+"--Params--"+postParam);
                }
                else {
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    Log.d(EVENT_TAG,"Driver Register Draft Get -url--"+URL+"--Params--"+postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {

                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "Register DRAFT RESPONSE: " + res);
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return res;
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
                Toast.makeText(FirstRegisterActivity.this, "Drafted", Toast.LENGTH_SHORT).show();
                new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
        }
    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream inputStream = getApplicationContext().getAssets().open("samplejson.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        System.out.println("JSON RESULTS:" + json);
        return json;
    }

    private String loadJSONFromAPI(){
        final StringBuilder stringBuilder = new StringBuilder();
        @SuppressLint("StaticFieldLeak")
        class PlaceDataAsync extends AsyncTask<String, String, String> {
            HttpGetData objHttpGetData;
            String URL;
            String res;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
            @Override
            protected String doInBackground(String... arg) {
                try {
                    InputStream inputStream = null;
                    URL=arg[0];
                    res = "";
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    if (inputStream != null) {
                        ConvertSteam objConvertSteam;
                        objConvertSteam = new ConvertSteam();
                        res = objConvertSteam.ConvertSteamToString(inputStream);
                    }
                    else {
                        Log.i("PlaceGET", "Not Ready");
                    }
                    if (res.trim().length() == 0) {
                        Log.i("PlaceGET", "Check Network" + res);
                    }
                    Log.i("PlaceGET", "Register DRAFT RESPONSE: " + res);
                }
                catch (Exception e) {
                    Log.i("PlaceGET", "doInBackground: Exception-- " + e.getMessage());
                    e.printStackTrace();
                }
                return res;
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
            }

            @Override
            protected void onPostExecute(String res) {
                super.onPostExecute(res);
                stringBuilder.append(res);
            }
        }

        PlaceDataAsync placeDataAsync = new PlaceDataAsync();
        placeDataAsync.execute(stateDistrictURL,"Get");
        System.out.println("RETURN VALUE: " + stringBuilder.toString());
        return stringBuilder.toString();
    }
}
