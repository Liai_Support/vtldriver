package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.lia.vtl.R;
import com.lia.vtl.adapter.ViewPhotoAdapter;
import com.lia.vtl.bo.MultiImg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewPhoto extends AppCompatActivity {
    private List<MultiImg> list_img;
    private RecyclerView irv;
    private GridLayoutManager pgm;
    private ViewPhotoAdapter viewPhotoAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);
        irv=(RecyclerView)findViewById(R.id.recycler_view);
        pgm=new GridLayoutManager(this,2,GridLayoutManager.VERTICAL, false);
        irv.setLayoutManager(pgm);
        list_img=new ArrayList<>();
        try {
            JSONArray   dataArray =new JSONArray(getIntent().getStringExtra("ImgName"));
            Log.d("detailsactasd",""+dataArray);
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject multiimgobj = dataArray.getJSONObject(i);
                MultiImg multiimg = new MultiImg(multiimgobj.getString("imgName"));
                list_img.add(multiimg);
                setupMultiImgData(list_img);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupMultiImgData(List<MultiImg> list_img) {
        viewPhotoAdapter=new ViewPhotoAdapter(list_img,ViewPhoto.this);
        irv.setAdapter(viewPhotoAdapter);
    }
}