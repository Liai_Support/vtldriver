package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lia.vtl.R;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.utility.VTLUtil;
import com.msg91.sendotpandroid.library.internal.Iso2Phone;
import com.msg91.sendotpandroid.library.utils.PhoneNumberFormattingTextWatcher;
import com.msg91.sendotpandroid.library.utils.PhoneNumberUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public static final String INTENT_PHONENUMBER = "9042663799";
    public static final String INTENT_COUNTRY_CODE = "+91";
    private EditText mPhoneNumber;
    private TextView mSmsButton;
    private String mCountryIso;
    private TextWatcher mNumberTextWatcher;
    private String mobileNumber;
    private VTLUtil __UTIL = VTLUtil.getInstance();
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPhoneNumber = findViewById(R.id.phoneNumber);
        mSmsButton = findViewById(R.id.smsVerificationButton);
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("TAG", "FCM Registration Token: " + token);
        __UTIL.setContext(this);
        mCountryIso = PhoneNumberUtils.getDefaultCountryIso(this);
        builder = new AlertDialog.Builder(this);
        final String defaultCountryName = new Locale("", mCountryIso).getDisplayName();
        final CountrySpinner spinner = (CountrySpinner) findViewById(R.id.spinner);
        spinner.init(defaultCountryName);
        spinner.addCountryIsoSelectedListener(new CountrySpinner.CountryIsoSelectedListener() {
            @Override
            public void onCountryIsoSelected(String selectedIso) {
                if (selectedIso != null) {
                    mCountryIso = selectedIso;
                    resetNumberTextWatcher(mCountryIso);
                    // force update:
                    mNumberTextWatcher.afterTextChanged(mPhoneNumber.getText());
                }
            }
        });
        resetNumberTextWatcher(mCountryIso);
        tryAndPrefillPhoneNumber();
    }

    private void tryAndPrefillPhoneNumber() {
        if (checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mPhoneNumber.setText(manager.getLine1Number());
        }
        else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            tryAndPrefillPhoneNumber();
        }
        else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {
                Toast.makeText(this, "This application needs permission to read your phone number to automatically "
                        + "pre-fill it", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void openActivity(String phoneNumber) {
        //new MainActivity.execute(StaticInfo.getcheckMobileUrl(mPhoneNumber.getText()));
         mobileNumber = phoneNumber;
         if (phoneNumber.length()==10) {
             new CheckMobileNumber().execute(StaticInfo.checkMobileNumber(phoneNumber));
         }
         else{
             InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
             imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
             Log.d("phoneNumber",""+phoneNumber);
             Toast.makeText(this, "Enter Valid 10 digit mobile no", Toast.LENGTH_LONG).show();
         }
    }

    private void setButtonsEnabled(boolean enabled) {
        mSmsButton.setEnabled(enabled);
    }

    public void onButtonClicked(View view) {
        Log.d("phoneNumber",""+mPhoneNumber.getText().toString());
            openActivity(getE164Number());
    }

    private void resetNumberTextWatcher(String countryIso) {
        if (mNumberTextWatcher != null) {
            mPhoneNumber.removeTextChangedListener(mNumberTextWatcher);
        }
        mNumberTextWatcher = new PhoneNumberFormattingTextWatcher(countryIso) {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }
            @SuppressLint("ResourceAsColor")
            @Override
            public synchronized void afterTextChanged(Editable s) {
                super.afterTextChanged(s);
                if (isPossiblePhoneNumber()) {
                    setButtonsEnabled(true);
                    mSmsButton.setAlpha(1f);
                    mPhoneNumber.setTextColor(R.color.appColor);
                }
                else {
                    setButtonsEnabled(false);
                    mSmsButton.setAlpha(0.5f);
                    mPhoneNumber.setTextColor(Color.RED);
                }
            }
        };
        mPhoneNumber.addTextChangedListener(mNumberTextWatcher);
    }

    private boolean isPossiblePhoneNumber() {
        return PhoneNumberUtils.isPossibleNumber(mPhoneNumber.getText().toString(), mCountryIso);
    }

    private String getE164Number() {
        return mPhoneNumber.getText().toString().replaceAll("\\D", "").trim();
    }

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Toast.makeText(getBaseContext(), "Received:"+result, Toast.LENGTH_LONG).show();
                if(result.equals("0")){
                    Toast.makeText(getBaseContext(), "Invalid Credentials 1", Toast.LENGTH_LONG).show();
                }
                else if(result.indexOf("success::")!=-1){
                    Log.d("received",result);
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    Toast.makeText(getBaseContext(), "Authentication successfully", Toast.LENGTH_LONG).show();
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Driver>>() {});
                    StaticInfo.driver=  driverlist.get(0);
                    StaticInfo.userId= StaticInfo.driver.getId();
                    Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getBaseContext(), Technav.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    @SuppressLint("StaticFieldLeak")
    private class  CheckMobileNumber extends AsyncTask<String,String,String>{
        LoadingDialog loadingDialog=new LoadingDialog(MainActivity.this);
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog.startLoadingDialog();
        }

        @Override
        protected void onPostExecute(String result) {
           try {
                Log.d("Result",result);
               loadingDialog.dismissDialog();
                if(result.equalsIgnoreCase("not")){
                    if (getIntent().getStringExtra("dactivity").equalsIgnoreCase("signup")) {
                        Intent verification = new Intent(MainActivity.this, VerificationActivity.class);
                        verification.putExtra(INTENT_PHONENUMBER, mobileNumber);
                        verification.putExtra("dactivity", "signup");
                        verification.putExtra(INTENT_COUNTRY_CODE, Iso2Phone.getPhone(mCountryIso));
                        startActivity(verification);
                    }
                    if (getIntent().getStringExtra("dactivity").equalsIgnoreCase("forgot")) {
                        Toast.makeText(getBaseContext(), "Mobile Number Not Registered!", Toast.LENGTH_LONG).show();
                    }
                }
                else{
                        String forgotPWD = getIntent().getStringExtra("dactivity");
                        int userId = Integer.parseInt(result.substring(result.lastIndexOf(" ") + 1));;
                        if(forgotPWD!=null && forgotPWD.equalsIgnoreCase("forgot")){
                            Intent intent = new Intent(getBaseContext(), VerificationActivity.class);
                            intent.putExtra("dactivity","forgot");
                            intent.putExtra("fuserid",userId);
                            System.out.println("Forgot Password Details:" + forgotPWD + ","+userId);
                            intent.putExtra(INTENT_PHONENUMBER, mobileNumber);
                            intent.putExtra(INTENT_COUNTRY_CODE, Iso2Phone.getPhone(mCountryIso));
                            startActivity(intent);
                        }
                        else{
                            mobileNoAuthWarning();
                        }
                }
            }
           catch (Exception e) {
                e.printStackTrace();
            }
        }
        }

    private void mobileNoAuthWarning() {
        builder.setMessage("Mobile Number already Exists..")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        dialog.cancel();
                        finish();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("VTL Driver");
        alert.show();
    }
}



