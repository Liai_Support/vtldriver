package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.Toolbar;
import android.widget.VideoView;

import com.lia.vtl.R;

public class TripHome extends AppCompatActivity implements  View.OnClickListener {
    private CardView tripcardview,paymentcardview;
    private VideoView tripVideoView;
    MediaController mediaControls;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_home);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tripcardview=(CardView)findViewById(R.id.tripcard);
        paymentcardview=(CardView)findViewById(R.id.paymencard);
        tripcardview.setOnClickListener(this);
        paymentcardview.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if (view==tripcardview){
            Intent i=new Intent(this,TripActivity.class);
            startActivity(i);
        }
        else if (view==paymentcardview){
            Intent i=new Intent(this,TransactionActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(TripHome.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}