package com.lia.vtl.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.lia.vtl.R;
import com.lia.vtl.adapter.CompletedTripAdapter;
import com.lia.vtl.adapter.ExpensesAdapter;
import com.lia.vtl.adapter.TransactionAdapter;
import com.lia.vtl.bo.CompletedTripBo;
import com.lia.vtl.bo.Expenses;
import com.lia.vtl.bo.TransactionBo;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.utility.VTLUtil;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class OngoingTrip extends Fragment implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    int dummyColor;
    private TextView fromaddrs,toaddrss,fromdate,advpayment,pickuptxt,dropofftxt,distancetxt,triptypetxt,viewmore,viewless,
            vname,vtype,vregno,vpermit,expandviewtxt,saveexpendtxt,unloadvechpic,loadedvechpic,podvecpic,tolltxt,repairtxt,unloadviewphoto,loadviewphoto,
    miscelltxt,expenbtn,tolltxtbtn,repairtxtbtn,miscelltxtbtn,miscelldesc,tripidtxt,tripPaymenttxt,acptbtntxt,rejectbtntxt,nodatatxt,paystatus;
    private LinearLayout extendlinear,expandviewlin,tollchargeLinear,repairLinear,miscellanusLinear,acceptedbelowLinear,actionbtnLinear;
    private RelativeLayout relativeLinear;
    private CardView card2view,card3view,card4view;
    private ImageView loadpicview,tollpicview,repairpicview,miscellpicview;
    private VTLUtil __UTIL = VTLUtil.getInstance();
    public static ArrayList<String> unloadpicList = new ArrayList<String>();
    public static ArrayList<String> loadpicList = new ArrayList<String>();
    private  String tollSelectedPic,repairSelectedPic;
    private boolean unloadedPic,loadedPic,tollChargePic,repairChargePic;
    LinearLayout linearLayout1;
    PopupWindow popupWindow;
    private static String imgBaseStr;
    private String status,currenttripId;
    public String selectedExpense;
    private File photoFile;
    String currentPhotoPath;
    public static final int CAMERA_REQUEST_CODE = 102;
    private  JSONArray unloadpicArray=new JSONArray();
    private  JSONArray loadpicArray=new JSONArray();
    public TransferUtility transferUtility;
    List<String> listing;
    private int percentage;
    private ACProgressFlower progressFlower;
    private String Url="ongoingTrip";
    private String updateUrl="updateStatus";
    private String addExpensesUrl="addExpenses";
    private String uploadTripimagesUrl="uploadTripimages";
    private Spinner expensespinner;
    private ArrayList<Expenses> list_expense;
    private ExpensesAdapter expensesAdapter;
    private Session session;
    private GridLayoutManager brgm;
    private RecyclerView expesesrcv;
    public OngoingTrip() {
    }
    @SuppressLint("ValidFragment")
    public OngoingTrip(int color) {
        this.dummyColor = color;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_ongoing_trip, container, false);
        if(__UTIL.getContext()==null){
            __UTIL.setContext(getActivity());
        }
        JSONObject obj=new JSONObject();
        try {
            obj.put("driverId", StaticInfo.userId);
            getInitialValues(String.valueOf(obj));
            Log.d("initializeuserId",""+obj);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        fromaddrs=view.findViewById(R.id.faddrestxt);
        toaddrss=view.findViewById(R.id.toaddrstxt);
        fromdate=view.findViewById(R.id.fadddate);
        tripPaymenttxt=view.findViewById(R.id.trippayment);
        viewmore=view.findViewById(R.id.viewmoretxt);
        viewless=view.findViewById(R.id.viewlesstxt);
        pickuptxt=view.findViewById(R.id.pickuptxt);
        dropofftxt=view.findViewById(R.id.dropofftxt);
        distancetxt=view.findViewById(R.id.distancetxt);
        triptypetxt=view.findViewById(R.id.triptypetxt);
        vname=view.findViewById(R.id.vechnametxt);
        vtype=view.findViewById(R.id.vechtypext);
        vregno=view.findViewById(R.id.vechregnotxt);
        tripidtxt=view.findViewById(R.id.tripId);
        vpermit=view.findViewById(R.id.permittxt);
        expandviewtxt=view.findViewById(R.id.expandview);
        saveexpendtxt=view.findViewById(R.id.saveexpend);
        unloadvechpic=view.findViewById(R.id.takeunloadvecpic);
        loadedvechpic=view.findViewById(R.id.takeloadedvecpic);
        paystatus=view.findViewById(R.id.paystatus);
        tollpicview=view.findViewById(R.id.tollimg);
        tolltxt=view.findViewById(R.id.tolltext);
        tolltxtbtn=view.findViewById(R.id.taketollchargepicbtn);
        repairpicview=view.findViewById(R.id.repairimg);
        repairtxt=view.findViewById(R.id.repairtext);
        repairtxtbtn=view.findViewById(R.id.takerepairpicbtn);
        miscelldesc=view.findViewById(R.id.misclidesc);
        miscelltxt=view.findViewById(R.id.misclitext);
        miscelltxtbtn=view.findViewById(R.id.takemislipicbtn);
        acptbtntxt=view.findViewById(R.id.acptbtn);
        rejectbtntxt=view.findViewById(R.id.rejectbtn);
        nodatatxt=view.findViewById(R.id.nodata);
        unloadviewphoto=view.findViewById(R.id.unloadview);
        loadviewphoto=view.findViewById(R.id.loadview);
        tollchargeLinear=view.findViewById(R.id.tollchargelinear);
        repairLinear=view.findViewById(R.id.repairlinear);
        miscellanusLinear=view.findViewById(R.id.misclilinear);
        acceptedbelowLinear=view.findViewById(R.id.acceptedbelowlinear);
        actionbtnLinear=view.findViewById(R.id.actionbtnlinear);
        relativeLinear=view.findViewById(R.id.relative1);
        extendlinear=view.findViewById(R.id.linear5);
        expandviewlin=view.findViewById(R.id.expandviewlinear);
        card3view=view.findViewById(R.id.card3);
        card2view=view.findViewById(R.id.card2);
        card4view=view.findViewById(R.id.card4);
         expensespinner = view.findViewById(R.id.expensespinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.expense_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        expensespinner.setAdapter(adapter);
        expensespinner.setOnItemSelectedListener(this);
        viewmore.setOnClickListener(this);
        viewless.setOnClickListener(this);
        expandviewtxt.setOnClickListener(this);
        saveexpendtxt.setOnClickListener(this);
        unloadvechpic.setOnClickListener(this);
        loadedvechpic.setOnClickListener(this);
        repairtxtbtn.setOnClickListener(this);
        miscelltxtbtn.setOnClickListener(this);
        tolltxtbtn.setOnClickListener(this);
        unloadviewphoto.setOnClickListener(this);
        loadviewphoto.setOnClickListener(this);
        acptbtntxt.setOnClickListener(this);
        rejectbtntxt.setOnClickListener(this);
        expesesrcv=view.findViewById(R.id.expenserv);
        brgm=new GridLayoutManager(getActivity().getBaseContext(),1,GridLayoutManager.VERTICAL, false);
        expesesrcv.setLayoutManager(brgm);
        // callback method to call credentialsProvider method.
        __UTIL.s3credentialsProvider();
        // callback method to call the setTransferUtility method
      setTransferUtility();
        return view;
    }

    public void setTransferUtility() {
        transferUtility = new TransferUtility(__UTIL.s3Client, getActivity());
    }

    public void uploadFileToS3(String name, Uri contentUri){
        File storeDirectory = new File(currentPhotoPath);
        TransferObserver transferObserver = transferUtility.upload(
                __UTIL.bucket,     /* The bucket to upload to */
                name,    /* The key for the uploaded object */
                storeDirectory       /* The file where the data to upload exists */
        );
        transferObserverListener(transferObserver);
    }


    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d("cpath",currentPhotoPath);
        return image;
    }

    @Override
    public void onClick(View view) {
        RelativeLayout rl = new RelativeLayout(this.getContext());
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (view==viewmore){
            viewmore.setVisibility(View.INVISIBLE);
            card3view.setVisibility(View.VISIBLE);
        }
        else if (view==viewless){
            viewmore.setVisibility(View.VISIBLE);
            card3view.setVisibility(View.GONE);
        }
        else if (view==expandviewtxt){
            if (expandviewlin.getVisibility()==View.VISIBLE) {
                expandviewlin.setVisibility(View.GONE);
                expensespinner.setVisibility(View.GONE);
                saveexpendtxt.setVisibility(View.GONE);
            }
            else{
                expandviewlin.setVisibility(View.VISIBLE);
                expensespinner.setVisibility(View.VISIBLE);
                saveexpendtxt.setVisibility(View.VISIBLE);
            }
        }
        else if (view==saveexpendtxt){
            JSONObject addexpensobj=new JSONObject();
            try {
                addexpensobj.put("tripId",""+currenttripId);
                JSONObject expensobj=new JSONObject();
                    if (selectedExpense== "TollCharge" || selectedExpense.equals("TollCharge")){
                        expensobj.put("tollCharges",tolltxt.getText().toString());
                    expensobj.put("tcImage",tollSelectedPic);
                    expensobj.put("repairCharges",0);
                    expensobj.put("repImage","");
                    expensobj.put("miscCharges",0);
                    expensobj.put("miscDet","");
                    addexpensobj.put("expDet",expensobj);
                    }
                    else if (selectedExpense== "Repair" || selectedExpense.equals("Repair")){
                        expensobj.put("tollCharges",0);
                        expensobj.put("tcImage","");
                        expensobj.put("repairCharges",repairtxt.getText().toString());
                        expensobj.put("repImage",repairSelectedPic);
                        expensobj.put("miscCharges",0);
                        expensobj.put("miscDet","");
                        addexpensobj.put("expDet",expensobj);
                    }
                    else if (selectedExpense== "Miscellaneous" || selectedExpense.equals("Miscellaneous")){
                        expensobj.put("tollCharges",0);
                        expensobj.put("tcImage","");
                        expensobj.put("repairCharges",0);
                        expensobj.put("repImage","");
                        expensobj.put("miscCharges",miscelltxt.getText().toString());
                        expensobj.put("miscDet",miscelldesc.getText().toString());
                        addexpensobj.put("expDet",expensobj);
                    }
                    addExpense(String.valueOf(addexpensobj));

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else if (view==podvecpic){
        }
        else if (view==tolltxtbtn){
        }
        else if (view==repairtxtbtn){
        }
        else if (view==miscelltxtbtn){
        }
        else if (view==acptbtntxt){
            JSONObject obj=new JSONObject();
            try {
                obj.put("tripId", currenttripId);
                obj.put("status", "accepted");
                getStatusUpdate(String.valueOf(obj));

                Log.d("statusupdate",""+obj);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else if (view==rejectbtntxt){
            JSONObject obj=new JSONObject();
            try {
                obj.put("tripId", currenttripId);
                obj.put("status", "rejected");
                getStatusUpdate(String.valueOf(obj));

                Log.d("statusupdate",""+obj);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
        else if (view==unloadviewphoto){
            Intent i=new Intent(getActivity(),ViewPhoto.class);
            i.putExtra("ImgName",""+unloadpicArray);
            startActivity(i);
        }
        else if (view==loadviewphoto){
            Intent i=new Intent(getActivity(),ViewPhoto.class);
            i.putExtra("ImgName",""+loadpicArray);
            startActivity(i);
        }
        switch (view.getId()) {
            case R.id.takeunloadvecpic:
                unloadedPic = true;
                loadedPic = false;
                tollChargePic=false;
                repairChargePic=false;
                takePicture();
                break;
            case R.id.takeloadedvecpic:
                unloadedPic = false;
                loadedPic = true;
                tollChargePic=false;
                repairChargePic=false;
                takePicture();
                break;
            case R.id.taketollchargepicbtn:
                unloadedPic = false;
                loadedPic = false;
                tollChargePic=true;
                repairChargePic=false;
                takePicture();
                break;
            case R.id.takerepairpicbtn:
                unloadedPic = false;
                loadedPic = false;
                tollChargePic=false;
                repairChargePic=true;
                takePicture();
                break;
        }
    }

    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(convertStandardJSONString(response));
                        if(obj.getString("error")=="true"){
                            relativeLinear.setVisibility(View.GONE);
                            nodatatxt.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            JSONObject dataobj=obj.getJSONObject("data");
                            Log.d("strrrr",response);
                                Log.d("strrrr",response);
                                actionbtnLinear.setVisibility(View.GONE);
                            tripidtxt.setText("Trip Id : "+dataobj.getString("tripId"));
                            currenttripId=""+dataobj.getString("tripId");
                            fromaddrs.setText(dataobj.getString("tFrom")+dataobj.getString("tFromcity"));
                            toaddrss.setText(dataobj.getString("tTo")+dataobj.getString("tTocity"));
                            tripPaymenttxt.setText(dataobj.getString("tripSalary"));
                            fromdate.setText("Date : "+dataobj.getString("movementDate"));
                            vname.setText(dataobj.getString("transporterName"));
                            vtype.setText(dataobj.getString("vType"));
                            vregno.setText(dataobj.getString("regNo"));
                            vpermit.setText(dataobj.getString("pertmitType"));
                            if (!dataobj.getString("unloaded").equals("")) {
                                if (dataobj.getJSONArray("unloaded") != null || dataobj.getJSONArray("unloaded").equals("")) {
                                    unloadpicArray = dataobj.getJSONArray("unloaded");
                                }
                            }
                            if (!dataobj.getString("loaded").equals("")) {
                                if (dataobj.getJSONArray("loaded") != null || dataobj.getJSONArray("loaded").equals("")) {
                                    loadpicArray = dataobj.getJSONArray("loaded");
                                }
                            }
                            if (dataobj.getString("driverPaymentstatus").equals("advpaid")) {
                                paystatus.setText("Paid");
                            }
                            else{
                                paystatus.setText(dataobj.getString("driverPaymentstatus"));
                            }
                            if (dataobj.getString("driverPaymentstatus").equals("unpaid")) {

                                paystatus.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                            }
                            else{
                                paystatus.setTextColor(ContextCompat.getColor(getContext(),R.color.green));
                            }
                            list_expense=new ArrayList<>();
                            JSONArray expenseArray=dataobj.getJSONArray("expenses");
                            for (int i = 0; i < expenseArray.length(); i++) {
                                JSONObject expIdobj = expenseArray.getJSONObject(i);
                                JSONObject expenseobj=expIdobj.getJSONObject("expdata");
                                Expenses expensesBo = new Expenses(expIdobj.getInt("expid"),expenseobj.getString("tollCharges"),
                                        expenseobj.getString("tcImage"),expenseobj.getString("repairCharges"),expenseobj.getString("repImage"),
                                        expenseobj.getString("miscCharges"),expenseobj.getString("miscDet"));
                                list_expense.add(expensesBo);
                            }
                            setupExpenseData(list_expense);
                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(error instanceof NoConnectionError){
                        ConnectivityManager cm = (ConnectivityManager)getContext()
                                .getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetwork = null;
                        if (cm != null) {
                            activeNetwork = cm.getActiveNetworkInfo();
                        }
                        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                            Toast.makeText(getActivity(), "Server is not connected to internet.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getActivity(), "Your device is not connected to internet.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if (error instanceof NetworkError || error.getCause() instanceof ConnectException
                            || (error.getCause().getMessage() != null
                            && error.getCause().getMessage().contains("connection"))){
                        Toast.makeText(getActivity(), "Your device is not connected to internet.",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (error.getCause() instanceof MalformedURLException){
                        Toast.makeText(getActivity(), "Bad Request.", Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof ParseError || error.getCause() instanceof IllegalStateException
                            || error.getCause() instanceof JSONException
                            || error.getCause() instanceof XmlPullParserException){
                        Toast.makeText(getActivity(), "Parse Error (because of invalid json or xml).",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (error.getCause() instanceof OutOfMemoryError){
                        Toast.makeText(getActivity(), "Out Of Memory Error.", Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof AuthFailureError){
                        Toast.makeText(getActivity(), "server couldn't find the authenticated request.",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
                        Toast.makeText(getActivity(), "Server is not responding.", Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof TimeoutError || error.getCause() instanceof SocketTimeoutException
                            || error.getCause() instanceof ConnectTimeoutException
                            || error.getCause() instanceof SocketException
                            || (error.getCause().getMessage() != null
                            && error.getCause().getMessage().contains("Connection timed out"))) {
                        Toast.makeText(getActivity(), "Connection timeout error",
                                Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(getActivity(), "An unknown error occurred.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void setupExpenseData(ArrayList<Expenses> list_expense) {
        expensesAdapter=new ExpensesAdapter(list_expense,getActivity().getBaseContext(),session,this);
        expesesrcv.setAdapter(expensesAdapter);
    }

    private void getStatusUpdate(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("qwerty",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+updateUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("strrrrsdfsd",response);
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            Intent i =new Intent(getActivity(),TripActivity.class);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void uploadTripimg(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("qwerty",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+uploadTripimagesUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            Intent i =new Intent(getActivity(),TripActivity.class);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addExpense(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(getActivity());
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("qwerty",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+addExpensesUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("strrrrsdfsd",response);
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            Toast.makeText(getActivity().getBaseContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                        }
                        else{
                            Intent i =new Intent(getActivity(),TripActivity.class);
                            startActivity(i);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            }
            catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.lia.vtl.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public String convertStandardJSONString(String data_json){
        data_json = data_json.replace("\"[", "[");
        data_json = data_json.replace("]\"", "]");
        return data_json;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                JSONObject unloadpicObj=new JSONObject();
                JSONObject loadpicObj=new JSONObject();
                if (unloadedPic) {
                    File f = new File(currentPhotoPath);
//                    unloadpicview.setImageURI(Uri.fromFile(f));
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    getActivity().sendBroadcast(mediaScanIntent);
                    uploadFileToS3(f.getName(), contentUri);
                    try {
                        unloadpicObj.put("imgName",f.getName());
                        unloadpicArray.put(unloadpicObj);
                        int len=unloadpicArray.length();
//                        {"tripId":2,"loaded":"","unloaded":"hi"}
                        JSONObject imguploadobj=new JSONObject();
                        imguploadobj.put("tripId",currenttripId);
                        imguploadobj.put("loaded","");
                        imguploadobj.put("unloaded",""+unloadpicArray);
                        uploadTripimg(""+imguploadobj);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if (loadedPic) {
                    File f = new File(currentPhotoPath);
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    getActivity().sendBroadcast(mediaScanIntent);
                    uploadFileToS3(f.getName(), contentUri);
                    try {
                        loadpicObj.put("imgName",f.getName());
                        loadpicArray.put(loadpicObj);
                        int len=loadpicArray.length();
                        JSONObject imguploadobj=new JSONObject();
                        imguploadobj.put("tripId",currenttripId);
                        imguploadobj.put("loaded",""+loadpicArray);
                        imguploadobj.put("unloaded","");
                        uploadTripimg(""+imguploadobj);
                        Log.d("wsdfghj",""+len+loadpicArray);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else if (tollChargePic) {
                    File f = new File(currentPhotoPath);
                    tollpicview.setImageURI(Uri.fromFile(f));
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    getActivity().sendBroadcast(mediaScanIntent);
                    uploadFileToS3(f.getName(), contentUri);
                    tollSelectedPic=f.getName();
                }
                else if (repairChargePic) {
                    File f = new File(currentPhotoPath);
                    repairpicview.setImageURI(Uri.fromFile(f));
                    Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri contentUri = Uri.fromFile(f);
                    mediaScanIntent.setData(contentUri);
                    getActivity().sendBroadcast(mediaScanIntent);
                    uploadFileToS3(f.getName(), contentUri);
                    repairSelectedPic=f.getName();
                }
            }
        }
    }

    private void progressFlowerDismiss(ACProgressFlower progressFlower) {
        if (progressFlower.isShowing()) {
            progressFlower.dismiss();
        }
    }

    private void loadingDialog() {
        progressFlower = new ACProgressFlower.Builder(getActivity())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("Please Wait..")
                .fadeColor(Color.DKGRAY).build();
        progressFlower.show();
    }

    public void transferObserverListener(TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState transferState) {
                switch (transferState) {
                    case IN_PROGRESS:
                        loadingDialog();
                        break;
                    case COMPLETED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case CANCELED:
                        break;
                    case FAILED:
                        progressFlowerDismiss(progressFlower);
                        break;
                    case PAUSED:
                        break;
                    case WAITING_FOR_NETWORK:
                        break;
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                percentage = (int) (bytesCurrent / bytesTotal * 100);
                Log.d("Loading Perc", "" + percentage);

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
                progressFlowerDismiss(progressFlower);

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
         selectedExpense = parent.getItemAtPosition(i).toString();
        Log.d("sdfgh",selectedExpense);
        if (selectedExpense== "TollCharge" || selectedExpense.equals("TollCharge")){
            tollchargeLinear.setVisibility(View.VISIBLE);
            repairLinear.setVisibility(view.GONE);
            miscellanusLinear.setVisibility(view.GONE);
        }
        else if (selectedExpense== "Repair" || selectedExpense.equals("Repair")){
            tollchargeLinear.setVisibility(View.GONE);
            repairLinear.setVisibility(view.VISIBLE);
            miscellanusLinear.setVisibility(view.GONE);
        }
        else if (selectedExpense== "Miscellaneous" || selectedExpense.equals("Miscellaneous")){
            tollchargeLinear.setVisibility(View.GONE);
            repairLinear.setVisibility(view.GONE);
            miscellanusLinear.setVisibility(view.VISIBLE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}