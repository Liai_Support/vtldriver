package com.lia.vtl.view;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.navigation.NavigationView;
//import com.lia.vtl.adapter.ArticleAdapter;
import com.lia.vtl.R;
import com.lia.vtl.bo.Article;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.utility.ConvertSteam;
import com.lia.vtl.utility.HttpGetData;
import com.lia.vtl.utility.HttpPostData;
import com.lia.vtl.utility.ImageUrlUtils;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.SessionManagement;
import com.lia.vtl.utility.StaticInfo;
//import com.lia.vtl.adapter.ArticleAdapter;
import com.lia.vtl.utility.VTLUtil;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
public class DashboardActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{
    private static final int PERMISSION_REQUEST_CODE = 200;
    public static int notificationCountCart = 0;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    SessionManagement session;
    private Session dsession;
    public boolean flag=true;
    Article[] listdata;
    ArticleAdapter adapter;
    RecyclerView recyclerView;
    private TextView petrolpriceval,dieselpriceval;
    private String Url="dailyFuelprice";
    private String tokenUrl="https://vtlpl.com/new_app/service/tech.php?action=updateDriverToken";
    private static Context mContext;
    private NavigationView navigationView;
    CardView partner,ainfo,myTrips,support;
    RelativeLayout beforeApprovel;
    GridLayout afterApprovel;
    TextView letsdrive;
    private Switch statusSwitch;
    private static String statusString;
    private VTLUtil __UTIL = VTLUtil.getInstance();
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        getFuelPrice();
        Log.d("TOken ",""+FirebaseInstanceId.getInstance().getToken());
        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dsession = new Session(this);
        mContext = DashboardActivity.this;
        __UTIL.setContext(this);
        checkPermission();
        new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
        builder = new AlertDialog.Builder(this);
        beforeApprovel = findViewById(R.id.before_approvel);
        afterApprovel =  findViewById(R.id.dashboardgrid);
        petrolpriceval=(TextView)findViewById(R.id.petrolpricetxt); 
        dieselpriceval=(TextView)findViewById(R.id.diselpricetxt); 
        letsdrive=(TextView)findViewById(R.id.letsdrivebtn);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_icon);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        partner =(CardView)findViewById(R.id.partner);
        ainfo =(CardView)findViewById(R.id.ainfo);
        myTrips = findViewById(R.id.my_trips);
        support = findViewById(R.id.support);
        statusSwitch =  findViewById(R.id.statusSwitch);
        partner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // . write your main code to execute, It will execute if the permission is already given.
                    Intent intent = new Intent(getBaseContext(), FirstRegisterActivity.class);
                    startActivity(intent);
            }
        });
        if (StaticInfo.driver.getStatus() == "A" || StaticInfo.driver.getStatus().equals("A") ||StaticInfo.driver.getStatus() == "V" || StaticInfo.driver.getStatus().equals("V") ) {
             ainfo.setVisibility(View.VISIBLE);
             myTrips.setVisibility(View.VISIBLE);
             support.setVisibility(View.VISIBLE);
            afterApprovel.setVisibility(View.VISIBLE);
            statusSwitch.setVisibility(View.VISIBLE);
             beforeApprovel.setVisibility(View.GONE);
            ainfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // . write your main code to execute, It will execute if the permission is already given.
                    Intent intent = new Intent(getBaseContext(), AdditionalInfo.class);
                    startActivity(intent);
                }
            });
        }
        else{
            afterApprovel.setVisibility(View.GONE);
            statusSwitch.setVisibility(View.GONE);
            beforeApprovel.setVisibility(View.VISIBLE);
        }
        statusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    statusSwitch.setText("Online");
                    statusString = "o";
                    new DriverStatusAsync().execute(StaticInfo.TEST_URL,"Post");
                }
                else {
                    statusSwitch.setText("Offline");
                    statusString = "f";
                    new DriverStatusAsync().execute(StaticInfo.TEST_URL,"Post");
                }
            }
        });

        letsdrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), FirstRegisterActivity.class));
            }
        });

        myTrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), TripHome.class);
                startActivity(intent);
            }
        });

        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this,AllottedVehicle.class);
                startActivity(intent);
            }
        });

        ImageUrlUtils imageUrlUtils = new ImageUrlUtils();
        ArrayList<String> cartlistImageUri =imageUrlUtils.getCartListImageUri();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerarticle);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);
        // specify an adapter (see also next example)
        Log.d("c", String.valueOf(StaticInfo.listArticle.size()));
        Article[] objects=new Article[StaticInfo.listArticle.size()];
        for(int i=0;i<StaticInfo.listArticle.size();i++){
            objects[i]=StaticInfo.listArticle.get(i);
        }
        adapter = new ArticleAdapter(objects);
        recyclerView.setAdapter(adapter);
        String token = FirebaseInstanceId.getInstance().getToken();
        JSONObject obj=new JSONObject();
        try {
            obj.put("driver_id",StaticInfo.userId);
            obj.put("token",token);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        getInitialValues(""+obj);
    }

    private void getFuelPrice() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestcartBody = "";
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.NEW_URL+Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    Log.d("strrrrsdfr", ">>" + obj);
                    if(obj.getString("error")=="true"){
                        Toast.makeText(getApplicationContext(),"Oops Error while fetch data",Toast.LENGTH_SHORT);
                    }
                    else{
                        JSONObject dataobj=obj.getJSONObject("data");
                        dataobj.getString("townName");
                        dataobj.getString("petrol");
                        dataobj.getString("diesel");
                        petrolpriceval.setText(""+dataobj.getString("petrol"));
                        dieselpriceval.setText(""+dataobj.getString("diesel"));
                            Log.d("asdfg", String.valueOf(dataobj.getString("petrol")+ "  , "+ dataobj.getString("diesel")));
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void commingSoon(String title) {
        builder.setMessage("Coming Soon..")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        }
                });
                  //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("VTL Driver: " +title);
        alert.show();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this,  Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
         ||ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            requestPermission();
        }
        return true;
    }

    private void requestPermission() {
         ActivityCompat.requestPermissions(this,
                new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    // main logic
                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED
                                ||ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(DashboardActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
     if (id == R.id.changepassword) {
            // ImageUrlUtils.getImageUrlsRate();
            startActivity(new Intent(DashboardActivity.this, ChangePassword.class));
            //startActivity(new Intent(DashboardActivity.this, AdditionalInfo.class));
        }
     else  if (id == R.id.logoutMenu) {
            // ImageUrlUtils.getImageUrlsRate();
            //startActivity(new Intent(MainActivity.this, RateCard.class));
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm ");
            builder.setMessage("Are you sure want to logout");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                     session.logoutUser();
//                    String token = FirebaseInstanceId.getInstance().getToken();
                    String token = FirebaseInstanceId.getInstance().getToken();
                    JSONObject obj=new JSONObject();
                    try {
                        obj.put("driver_id", StaticInfo.userId);
                        obj.put("token", token);
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getInitialValues(""+obj);
                    StaticInfo.userId=0;
                    startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Toast.makeText(getApplicationContext(), "You've changed your mind to delete all records", Toast.LENGTH_SHORT).show();
                }
            });
            builder.show();
        }
     else if(id==R.id.share){
         final String appPackageName = this.getPackageName();
         Intent sendIntent = new Intent();
         sendIntent.setAction(Intent.ACTION_SEND);
         sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + appPackageName);
         sendIntent.setType("text/plain");
         this.startActivity(sendIntent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        overridePendingTransition(0,0);
        return true;
    }

    private void getInitialValues(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("requesttoken",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,tokenUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("requesttoken",""+response);
                    if (response=="success"){

                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*** Web Servcice Client  ***/
    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncupdateTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("sdasjkdhksajdhkjsdf",result);
                if(result.equals("0")){
                    Toast.makeText(getBaseContext(), "Invalid Credentials ", Toast.LENGTH_LONG).show();
                }else if(result.indexOf("success::")!=-1){
                    Log.d("received",result);
                    result=result.replace("success::","");
                    Log.d("received1",result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Driver>>() {});
                    StaticInfo.driver=  driverlist.get(0);
                    dsession.setusename(""+result);
                    if (StaticInfo.driver.getoStatus()!=null) {
                        Log.d("onlineStatus",StaticInfo.driver.getoStatus());
                        if (StaticInfo.driver.getoStatus().equals("f")||StaticInfo.driver.getoStatus()=="f") {
                            statusSwitch.setChecked(false);
                        } else {
                            statusSwitch.setChecked(true);
                        }
                    }

                }
                else{
                    Log.d("session","error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public  class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder>{
        private String[] mDataset;
        private Article[] listdata;
        private String imgPath="";
        private Session session;
        public ArticleAdapter(Article[] listdata) {
            this.listdata = listdata;
        }
        @Override
        public ArticleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem= layoutInflater.inflate(R.layout.article_layout, parent, false);
            ArticleAdapter.ViewHolder viewHolder = new ArticleAdapter.ViewHolder(listItem);
            return viewHolder;
        }
        // Provide a suitable constructor (depends on the kind of dataset)
        public ArticleAdapter(String[] myDataset) {
            mDataset = myDataset;
        }

        @Override
        public  void onBindViewHolder(ArticleAdapter.ViewHolder holder, int position) {
            final Article myListData = listdata[position];
            Glide.with(DashboardActivity.this)
                    .load(StaticInfo.LocalImgPath+myListData.getAimage())
                    .placeholder(R.drawable.defaultimg)
                    .into(holder.imageView);
            Log.d("url",StaticInfo.LocalImgPath+myListData.getAimage());
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new ArticleAdapter.HttpAsyncArticleTask().execute(StaticInfo.getArticleByIdUrl(myListData.getId()));
                }
            });
        }

        @Override
        public int getItemCount() {
            return listdata.length;
        }

        public  class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView;
            public LinearLayout relativeLayout;
            public ViewHolder(View itemView) {
                super(itemView);
                this.imageView = (ImageView) itemView.findViewById(R.id.articleimg);
                relativeLayout = (LinearLayout)itemView.findViewById(R.id.articlelayout);
            }
        }

        private class HttpAsyncArticleTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... urls) {
                return GET(urls[0]);
            }
            @Override
            protected void onPostExecute(String result) {
                try {
                    if(result.equals("0")){
                    }
                    else if(result.indexOf("success::")!=-1){
                        Log.d("received",result);
                        result=result.replace("success::","");
                        Log.d("received1",result);
                        ObjectMapper mapper = new ObjectMapper();
                        ArrayList<Article> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Article>>() {});
                        StaticInfo.article=  driverlist.get(0);
                        dsession.setarticle(""+result);
                        Intent intent = new Intent(getBaseContext(), ArticleActivity.class);
                        startActivity(intent);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
            startActivity(intent);
            finish();
            System.exit(0);
        }
        else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();
    }

    @SuppressLint("StaticFieldLeak")
    public static class DriverStatusAsync extends AsyncTask<String, String, String> {
        HttpPostData objHttpPostData;
        HttpGetData objHttpGetData;
        String URL;
        String postParam = "";
        String EVENT_TAG = DriverStatusAsync.class.getSimpleName();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL=arg[0]+StaticInfo.DRIVER_STATUS;
                Log.i(EVENT_TAG, "URL: " + URL);
                String res = "";
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", StaticInfo.driver.getId());
                    jsonObject.put("driverStatus",statusString );
                }
                catch (JSONException e){
                  e.printStackTrace();
                }
                postParam = jsonObject.toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG,"Driver Status Post URL With  Params: "+postParam);
                }
                else {
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    Log.d(EVENT_TAG,"Driver Status Get URL With  Params: "+postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {

                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "Driver Status RESPONSE: " + res);
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return "";
        }
        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}