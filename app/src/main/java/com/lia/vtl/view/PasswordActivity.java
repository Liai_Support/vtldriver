package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lia.vtl.R;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class PasswordActivity extends AppCompatActivity  implements View.OnClickListener {
    EditText pswd, cpswd;
    Button btn;
    private String androidDeviceId;
    private Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        session = new Session(this);
        if (isConnected()) {
        }
        else {
            Toast.makeText(PasswordActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
        }
        btn = (Button) findViewById(R.id.reg_submit);
        pswd = (EditText) findViewById(R.id.pswdtxt);
        cpswd = (EditText) findViewById(R.id.cpswdtxt);
        btn.setOnClickListener(this);
        androidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void onClick(View view) {
        if (view == btn) {
            String passwordStr = pswd.getText().toString().trim();
            String repasswordStr = cpswd.getText().toString().trim();
            Log.d("pswd", passwordStr);
            Log.d("cpswd", repasswordStr);
            if (passwordStr.equals("") || repasswordStr.equals("") || passwordStr.equals("") || repasswordStr == null) {
                Toast.makeText(getApplicationContext(), "All field is mandatory", Toast.LENGTH_LONG).show();
            }
            else if (!passwordStr.equals(repasswordStr)) {
                String statusStr = "N";
                Log.d("mob", StaticInfo.getRegisterUrl(StaticInfo.mobNo, passwordStr, statusStr, androidDeviceId));
            }
            else if (passwordStr.equals(repasswordStr)) {
                String forgotPWD = getIntent().getStringExtra("dactivity");
                int userId = getIntent().getIntExtra("userId", 0);
                if (forgotPWD != null && forgotPWD.equalsIgnoreCase("forgot")) {
                    new ForgotPasswordAsync().execute(StaticInfo.getForgotPWDUrl(userId, passwordStr));
                }
                else {
                    String statusStr = "N";
                    new HttpAsyncTask().execute(StaticInfo.getRegisterUrl(StaticInfo.mobNo, passwordStr, statusStr, androidDeviceId));
                }
            }
        }
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }
    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            Log.d("received:", result);
            try {
                if (result.equals("success")) {
                    Toast.makeText(getBaseContext(), "successful.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(PasswordActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getBaseContext(), "Failure", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ForgotPasswordAsync extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
                Log.d("dfghjklsdfgh",result);
            try {
                if (result.equalsIgnoreCase("success")) {
                    startActivity(new Intent(PasswordActivity.this, LoginActivity.class));
                    Toast.makeText(getApplicationContext(), "Your Password Successfully Changed.", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getBaseContext(), "Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }
    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(PasswordActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}