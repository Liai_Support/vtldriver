package com.lia.vtl.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.vtl.R;
import com.lia.vtl.bo.Article;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.startup.ThanksActivity;
import com.lia.vtl.utility.ConvertSteam;
import com.lia.vtl.utility.HttpGetData;
import com.lia.vtl.utility.HttpPostData;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.utility.VTLUtil;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentInfo extends AppCompatActivity implements View.OnClickListener {
    EditText bankactxt;
    RadioGroup apayment, scycle, spcycle;
    RadioButton cash, btransfer, trip, weekly, monthly, spyes, spno;
    Button draft, submit;
    TextView btnprev, takePassBookPhoto;
    ImageView passbookImg;
    private Session session;
    public static String imgBaseStr="";
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private VTLUtil __UTIL = VTLUtil.getInstance();
    private File photoFile;
    String currentPhotoPath;
    public static final int CAMERA_REQUEST_CODE = 102;
    AmazonS3 s3Client;
    String bucket = "vtl-app";
    TransferUtility transferUtility;
    List<String> listing;
    private int percentage;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        session = new Session(this);
        bankactxt = (EditText) findViewById(R.id.bankac);
        cash = (RadioButton) findViewById(R.id.cash);
        btransfer = (RadioButton) findViewById(R.id.btransfer);
        weekly = (RadioButton) findViewById(R.id.weekly);
        trip = (RadioButton) findViewById(R.id.trip);
        monthly = (RadioButton) findViewById(R.id.monthly);
        spyes = (RadioButton) findViewById(R.id.spyes);
        spno = (RadioButton) findViewById(R.id.spno);
        draft = (Button) findViewById(R.id.draft);
        submit = (Button) findViewById(R.id.submit);
        btnprev = (TextView) findViewById(R.id.btn_next);
        takePassBookPhoto = findViewById(R.id.takepassbookpic);
        passbookImg = findViewById(R.id.passbookpic);
        progressBar = findViewById(R.id.progress);
        if (StaticInfo.driver.getPassbookImg() != null || StaticInfo.driver.getPassbookImg()!="") {
            progressBar.setVisibility(View.VISIBLE);
            passbookImg.setVisibility(View.GONE);
            Glide.with(this)
                    .load("https://vtl-app.s3.amazonaws.com/"+StaticInfo.driver.getPassbookImg()).placeholder(R.drawable.defaultimg)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            passbookImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            passbookImg.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .into(passbookImg);
            Log.d("asd", "https://vtl-app.s3.amazonaws.com/" + StaticInfo.driver.getPassbookImg());
        }
        else {
            passbookImg.setImageDrawable(getDrawable(R.drawable.defaultimg));
        }
        bankactxt.setText(StaticInfo.driver.getBankAcNo());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back_icon));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                Intent intent = new Intent(getBaseContext(), AdditionalInfo.class);
                startActivity(intent);
            }
        });
        if (StaticInfo.driver.getAdvancePayment() == null) {
        }
        else {
            if (StaticInfo.driver.getAdvancePayment().equals("cash")) {
                cash.setChecked(true);
            }
            else if (StaticInfo.driver.getAdvancePayment().equals("banktransfer")) {
                btransfer.setChecked(true);
            }
        }
        if (StaticInfo.driver.getSalaryCycle() == null) {

        }
        else if (StaticInfo.driver.getSalaryCycle().equals("trip")) {
            trip.setChecked(true);

        }
        else if (StaticInfo.driver.getSalaryCycle().equals("weekly")) {
            weekly.setChecked(true);

        }
        else if (StaticInfo.driver.getSalaryCycle().equals("monthly")) {
            monthly.setChecked(true);
        }
        if (StaticInfo.driver.getSplitPayment() == null) {

        }
        else if (StaticInfo.driver.getSplitPayment().equals("yes")) {
            spyes.setChecked(true);
        }
        else if (StaticInfo.driver.getSplitPayment().equals("no")) {
            spyes.setChecked(true);
        }
        if (StaticInfo.driver.getStatus() != null && StaticInfo.driver.getStatus().equalsIgnoreCase("A") || StaticInfo.driver.getStatus().equalsIgnoreCase("V")) {
            draft.setVisibility(View.GONE);
        }
        else {
            draft.setVisibility(View.VISIBLE);
        }
        // callback method to call credentialsProvider method.
        s3credentialsProvider();
        // callback method to call the setTransferUtility method
        setTransferUtility();
        cash.setOnClickListener(this);
        btransfer.setOnClickListener(this);
        trip.setOnClickListener(this);
        weekly.setOnClickListener(this);
        monthly.setOnClickListener(this);
        spyes.setOnClickListener(this);
        spno.setOnClickListener(this);
        draft.setOnClickListener(this);
        submit.setOnClickListener(this);
        btnprev.setOnClickListener(this);
        takePassBookPhoto.setOnClickListener(this);
        if (__UTIL.getContext() == null) {
            __UTIL.setContext(this);
        }
    }

    public void s3credentialsProvider() {
        // Initialize the AWS Credential
        CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider =
                new CognitoCachingCredentialsProvider(
                        getApplicationContext(),
                        "us-east-1:4f62338b-6520-4786-8530-b62c1146564f", // Identity pool ID
                        Regions.US_EAST_1 // Region
                );
        createAmazonS3Client(cognitoCachingCredentialsProvider);
    }

    /**
     * Create a AmazonS3Client constructor and pass the credentialsProvider.
     *
     * @param credentialsProvider
     */
    public void createAmazonS3Client(CognitoCachingCredentialsProvider
                                             credentialsProvider) {
        // Create an S3 client
        s3Client = new AmazonS3Client(credentialsProvider);
        // Set the region of your S3 bucket
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_1));
    }

    public void setTransferUtility() {
        transferUtility = new TransferUtility(s3Client, getApplicationContext());
    }

    public void uploadFileToS3(String name, Uri contentUri) {
        File storeDirectory = new File(currentPhotoPath);
        TransferObserver transferObserver = transferUtility.upload(
                bucket,     /* The bucket to upload to */
                name,    /* The key for the uploaded object */
                storeDirectory       /* The file where the data to upload exists */
        );
        transferObserverListener(transferObserver);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
//        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d("cpath", currentPhotoPath);
        return image;
    }

    @Override
    public void onClick(View view) {
        String bankacStr;
        if (view == cash) {
            StaticInfo.driver.setAdvancePayment("cash");
        }
        else if (view == btransfer) {
            StaticInfo.driver.setAdvancePayment("banktransfer");
        }
        else if (view == trip) {
            StaticInfo.driver.setSalaryCycle("trip");

        }
        else if (view == weekly) {
            StaticInfo.driver.setSalaryCycle("weekly");
        }
        else if (view == monthly) {
            StaticInfo.driver.setSalaryCycle("monthly");
        }
        else if (view == spyes) {
            StaticInfo.driver.setSplitPayment("yes");
        }
        else if (view == spno) {
            StaticInfo.driver.setSplitPayment("no");
        }
        else if (view == draft) {
            bankacStr = bankactxt.getText().toString().trim();
            StaticInfo.driver.setBankAcNo(bankacStr);
            StaticInfo.driver.setStatus("V");
            String URL_EXE = StaticInfo.getUpdateRegisterUrl(StaticInfo.driver.getId(), StaticInfo.driver.getUname(), StaticInfo.driver.getUserImg(),
                    StaticInfo.driver.getDob(), StaticInfo.driver.getGender(), StaticInfo.driver.getMaritalStatus(), StaticInfo.driver.getAddress(),
                    StaticInfo.driver.getCity(), StaticInfo.driver.getState(), StaticInfo.driver.getCountry(), StaticInfo.driver.getAdhar(),
                    StaticInfo.driver.getAadhaarImg(), StaticInfo.driver.getPancard(), StaticInfo.driver.getLicenseNo(), StaticInfo.driver.getLicenceFrontImg(),
                    StaticInfo.driver.getLicenceBackImg(), StaticInfo.driver.getLicenseExpiryDate(), StaticInfo.driver.getBatch(),
                    StaticInfo.driver.getBatchExpiry(), StaticInfo.driver.getDrivingExperience(), StaticInfo.driver.getVehicleHandling(),
                    StaticInfo.driver.getEmerContNo(), StaticInfo.driver.getInsurance(), StaticInfo.driver.getEmployeeType(),
                    StaticInfo.driver.getReferPersonName(), StaticInfo.driver.getReferContactNo(), bankacStr, StaticInfo.driver.getPassbookImg(), StaticInfo.driver.getAdvancePayment(),
                    StaticInfo.driver.getSalaryCycle(), StaticInfo.driver.getSplitPayment(), "V", StaticInfo.driver.getPincode());
            new PaymentReferenceDraftAsync().execute(StaticInfo.TEST_URL, "Post");
            Log.d("draft", URL_EXE);

        }
        else if (view == submit) {
            bankacStr = bankactxt.getText().toString().trim();
            StaticInfo.driver.setBankAcNo(bankacStr);
            StaticInfo.driver.setStatus("V");
            if (StaticInfo.driver.getId() != 0 && !StaticInfo.driver.getUname().equals("") && !StaticInfo.driver.getDob().equals("")
                    && !StaticInfo.driver.getGender().equals("") && !StaticInfo.driver.getMaritalStatus().equals("") &&
                    !StaticInfo.driver.getCity().equals("") && !StaticInfo.driver.getState().equals("") &&
                    !StaticInfo.driver.getCountry().equals("") && !StaticInfo.driver.getAdhar().equals("") &&
                    !StaticInfo.driver.getLicenseNo().equals("") && !StaticInfo.driver.getDrivingExperience().equals("") &&
                    !StaticInfo.driver.getVehicleHandling().equals("") && !StaticInfo.driver.getEmerContNo().equals("") &&
                    !StaticInfo.driver.getEmployeeType().equals("") &&
                    !StaticInfo.driver.getReferPersonName().equals("") && !StaticInfo.driver.getReferContactNo().equals("") &&
                    !bankactxt.getText().toString().equals("") && !StaticInfo.driver.getAdvancePayment().equals("") &&
                    !StaticInfo.driver.getSalaryCycle().equals("") && !StaticInfo.driver.getSplitPayment().equals("")) {
                new PaymentReferenceSubmitAsync().execute(StaticInfo.TEST_URL, "Post");
            }
            else {
                Toast.makeText(getBaseContext(), "All the Fields are Mandatory", Toast.LENGTH_LONG).show();
            }
        }
        else if (view == btnprev) {
            bankactxt = (EditText) findViewById(R.id.bankac);
            bankacStr = bankactxt.getText().toString().trim();
            StaticInfo.driver.setBankAcNo(bankacStr);
            Log.d("banstr", StaticInfo.driver.getBankAcNo());
            Intent intent = new Intent(getBaseContext(), AdditionalInfo.class);
            startActivity(intent);
        }
        else if (view == takePassBookPhoto) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                }
                catch (IOException ex) {
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.lia.vtl.android.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
                }
            }
        }
    }

    public static String GET(String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        }
        catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            Log.d("received:", result);
            try {
                if (result.equals("success")) {
                    Toast.makeText(getBaseContext(), " successful.", Toast.LENGTH_LONG).show();
                    new HttpAsyncupdateTask().execute(StaticInfo.getUpdateRegisterUrl(StaticInfo.driver.getId(), StaticInfo.driver.getUname(), StaticInfo.driver.getUserImg(), StaticInfo.driver.getDob(), StaticInfo.driver.getGender(), StaticInfo.driver.getMaritalStatus(), StaticInfo.driver.getAddress(), StaticInfo.driver.getCity(), StaticInfo.driver.getState(), StaticInfo.driver.getCountry(), StaticInfo.driver.getAdhar(), StaticInfo.driver.getAadhaarImg(), StaticInfo.driver.getPancard(), StaticInfo.driver.getLicenseNo(), StaticInfo.driver.getLicenceFrontImg(), StaticInfo.driver.getLicenceBackImg(), StaticInfo.driver.getLicenseExpiryDate(), StaticInfo.driver.getBatch(), StaticInfo.driver.getBatchExpiry(), StaticInfo.driver.getDrivingExperience(), StaticInfo.driver.getVehicleHandling(), StaticInfo.driver.getEmerContNo(), StaticInfo.driver.getInsurance(), StaticInfo.driver.getEmployeeType(), StaticInfo.driver.getReferPersonName(), StaticInfo.driver.getReferContactNo(), StaticInfo.driver.getBankAcNo(), StaticInfo.driver.getPassbookImg(), StaticInfo.driver.getAdvancePayment(), StaticInfo.driver.getSalaryCycle(), StaticInfo.driver.getSplitPayment(), StaticInfo.driver.getStatus(), StaticInfo.driver.getPincode()));
                }
                else {
                    Toast.makeText(getBaseContext(), " Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncSubmitTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                if (result.equals("success")) {
                    new HttpAsyncSubmtupdateTask().execute(StaticInfo.getUpdateRegisterUrl(StaticInfo.driver.getId(), StaticInfo.driver.getUname(), StaticInfo.driver.getUserImg(), StaticInfo.driver.getDob(), StaticInfo.driver.getGender(), StaticInfo.driver.getMaritalStatus(), StaticInfo.driver.getAddress(), StaticInfo.driver.getCity(), StaticInfo.driver.getState(), StaticInfo.driver.getCountry(), StaticInfo.driver.getAdhar(), StaticInfo.driver.getAadhaarImg(), StaticInfo.driver.getPancard(), StaticInfo.driver.getLicenseNo(), StaticInfo.driver.getLicenceFrontImg(), StaticInfo.driver.getLicenceBackImg(), StaticInfo.driver.getLicenseExpiryDate(), StaticInfo.driver.getBatch(), StaticInfo.driver.getBatchExpiry(), StaticInfo.driver.getDrivingExperience(), StaticInfo.driver.getVehicleHandling(), StaticInfo.driver.getEmerContNo(), StaticInfo.driver.getInsurance(), StaticInfo.driver.getEmployeeType(), StaticInfo.driver.getReferPersonName(), StaticInfo.driver.getReferContactNo(), StaticInfo.driver.getBankAcNo(), StaticInfo.driver.getPassbookImg(), StaticInfo.driver.getAdvancePayment(), StaticInfo.driver.getSalaryCycle(), StaticInfo.driver.getSplitPayment(), StaticInfo.driver.getStatus(), StaticInfo.driver.getPincode()));
                    Intent intent = new Intent(PaymentInfo.this, ThanksActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getBaseContext(), " Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncupdateTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s", result);
                if (result.indexOf("success::") != -1) {
                    Log.d("received", result);
                    result = result.replace("success::", "");
                    Log.d("received1", result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist = mapper.readValue(result, new TypeReference<ArrayList<Driver>>() {
                    });
                    StaticInfo.driver = driverlist.get(0);
                    session.setusename("" + result);
                    new HttpAsyncArticleTask().execute(StaticInfo.getArticleUrl());
                }
                else {
                    Log.d("session", "error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncSubmtupdateTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                Log.d("s", result);
                if (result.indexOf("success::") != -1) {
                    Log.d("received", result);
                    result = result.replace("success::", "");
                    Log.d("received1", result);
                    ObjectMapper mapper = new ObjectMapper();
                    ArrayList<Driver> driverlist = mapper.readValue(result, new TypeReference<ArrayList<Driver>>() {
                    });
                    StaticInfo.driver = driverlist.get(0);
                    session.setusename("" + result);
                    new HttpAsyncArticleTask().execute(StaticInfo.getArticleUrl());
                    Intent intent = new Intent(PaymentInfo.this, ThanksActivity.class);
                    startActivity(intent);
                }
                else {
                    Log.d("session", "error on update session");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class HttpAsyncArticleTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                StaticInfo.listArticle = mapper.readValue(result, new TypeReference<ArrayList<Article>>() {
                });
                StaticInfo.articleList = StaticInfo.listArticle.get(0);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class PaymentReferenceDraftAsync extends AsyncTask<String, String, String> {
        HttpPostData objHttpPostData;
        HttpGetData objHttpGetData;
        String URL;
        String postParam = "";
        String EVENT_TAG = PaymentReferenceDraftAsync.class.getSimpleName();
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL = arg[0] + StaticInfo.DRIVER_UPDATE;
                Log.i(EVENT_TAG, "URL: " + URL);
                String res = "";
                postParam = jsonObject().toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG, "Payment Draft Post--url--" + URL + "--Params--" + postParam);
                }
                else {
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    Log.d(EVENT_TAG, "Payment Draft Get -url--" + URL + "--Params--" + postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {
                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "Payment Draft RESPONSE: " + res);
                if (res.equals("success")) {
                    Toast.makeText(getBaseContext(), "successful.", Toast.LENGTH_LONG).show();
                    new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                }
                else {
                    Toast.makeText(getBaseContext(), "Registration Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return "";
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class PaymentReferenceSubmitAsync extends AsyncTask<String, String, String> {
        HttpPostData objHttpPostData;
        HttpGetData objHttpGetData;
        String URL;
        String postParam = "";
        String EVENT_TAG = PaymentReferenceSubmitAsync.class.getSimpleName();
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(String... arg) {
            try {
                InputStream inputStream = null;
                URL = arg[0] + StaticInfo.DRIVER_UPDATE;
                Log.i(EVENT_TAG, "URL: " + URL);
                res = "";
                postParam = jsonObject().toString();
                if (arg[1].equals("Post")) {
                    objHttpPostData = new HttpPostData();
                    inputStream = objHttpPostData.ByPostMethod(URL, postParam);
                    Log.d(EVENT_TAG, "Payment Submit Post--url--" + URL + "--Params--" + postParam);
                }
                else {
                    objHttpGetData = new HttpGetData();
                    inputStream = objHttpGetData.ByGetMethod(URL);
                    Log.d(EVENT_TAG, "Payment Submit Get -url--" + URL + "--Params--" + postParam);
                }
                if (inputStream != null) {
                    ConvertSteam objConvertSteam;
                    objConvertSteam = new ConvertSteam();
                    res = objConvertSteam.ConvertSteamToString(inputStream);
                }
                else {
                }
                if (res.trim().length() == 0) {
                    Log.i(EVENT_TAG, "Check Network" + res);
                }
                Log.i(EVENT_TAG, "Payment Submit RESPONSE: " + res);
                if (res.equals("success")) {
                    Toast.makeText(getBaseContext(), "Successful.", Toast.LENGTH_LONG).show();
                    new HttpAsyncupdateTask().execute(StaticInfo.getDriverDetailsUrl(StaticInfo.driver.getId()));
                }
                else {
                    Toast.makeText(getBaseContext(), "Registration Failure", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e) {
                Log.i(EVENT_TAG, "doInBackground: Exception-- " + e.getMessage());
                e.printStackTrace();
            }
            return res;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equalsIgnoreCase("success")) {
                Intent intent = new Intent(PaymentInfo.this, DashboardActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(currentPhotoPath);
                Glide.with(this)
                        .load(Uri.fromFile(f))
                        .into(passbookImg);
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                imgBaseStr=f.getName();
                uploadFileToS3(f.getName(), contentUri);
            }
        }
    }

    public static JSONObject jsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", StaticInfo.driver.getId());
            jsonObject.put("uname", StaticInfo.driver.getUname());
            jsonObject.put("userImg", StaticInfo.driver.getUserImg());
            jsonObject.put("dob", StaticInfo.driver.getDob());
            jsonObject.put("gender", StaticInfo.driver.getGender());
            jsonObject.put("maritalStatus", StaticInfo.driver.getMaritalStatus());
            jsonObject.put("address", StaticInfo.driver.getAddress());
            jsonObject.put("city", StaticInfo.driver.getCity());
            jsonObject.put("state", StaticInfo.driver.getState());
            jsonObject.put("country", StaticInfo.driver.getCountry());
            jsonObject.put("adhar", StaticInfo.driver.getAdhar());
            jsonObject.put("aadhaarImg", StaticInfo.driver.getAadhaarImg());
            jsonObject.put("pancard", StaticInfo.driver.getPancard());
            jsonObject.put("licenseNo", StaticInfo.driver.getLicenseNo());
            jsonObject.put("licenceFrontImg", StaticInfo.driver.getLicenceFrontImg());
            jsonObject.put("licenceBackImg", StaticInfo.driver.getLicenceBackImg());
            jsonObject.put("licenseExpiryDate", StaticInfo.driver.getLicenseExpiryDate());
            jsonObject.put("batch", StaticInfo.driver.getBatch());
            jsonObject.put("batchExpiry", StaticInfo.driver.getBatchExpiry());
            jsonObject.put("drivingExperience", StaticInfo.driver.getDrivingExperience());
            jsonObject.put("vehicleHandling", StaticInfo.driver.getVehicleHandling());
            jsonObject.put("insurance", StaticInfo.driver.getInsurance());
            jsonObject.put("emerContNo", StaticInfo.driver.getEmerContNo());
            jsonObject.put("employeeType", StaticInfo.driver.getEmployeeType());
            jsonObject.put("referPersonName", StaticInfo.driver.getReferPersonName());
            jsonObject.put("referContactNo", StaticInfo.driver.getReferContactNo());
            jsonObject.put("bankAcNo", StaticInfo.driver.getBankAcNo());
            jsonObject.put("passbookImg", imgBaseStr);
            jsonObject.put("advancePayment", StaticInfo.driver.getAdvancePayment());
            jsonObject.put("salaryCycle", StaticInfo.driver.getSalaryCycle());
            jsonObject.put("splitPayment", StaticInfo.driver.getSplitPayment());
            jsonObject.put("status", StaticInfo.driver.getStatus());
            jsonObject.put("pincode", StaticInfo.driver.getPincode());
        }
        catch (JSONException e) {
            e.getMessage();
        }
        return jsonObject;
    }
    /**
     * This is listener method of the TransferObserver
     * Within this listener method, we get status of uploading and downloading file,
     * to display percentage of the part of file to be uploaded or downloaded to S3
     * It displays an error, when there is a problem in  uploading or downloading file to or from S3.
     *
     * @param transferObserver
     */
    public void transferObserverListener(TransferObserver transferObserver) {
        transferObserver.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                Toast.makeText(getApplicationContext(), "State Change"
                        + state, Toast.LENGTH_SHORT).show();
                Log.d("state", "" + state);
                String sta = String.valueOf(state);
            }
            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                percentage = (int) (bytesCurrent / bytesTotal * 100);
                Toast.makeText(getApplicationContext(), "Progress in %"
                        + percentage, Toast.LENGTH_SHORT).show();
                Log.d("perc", "" + percentage);
            }
            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
            }

        });
    }
}