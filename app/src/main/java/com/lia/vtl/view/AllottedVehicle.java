package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.lia.vtl.R;
import com.lia.vtl.bo.Expenses;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;

import static com.lia.vtl.utility.VTLUtil.getContext;

public class AllottedVehicle extends AppCompatActivity implements View.OnClickListener {
    private TextView vechno,vectype,insurebtn,fcbtn,rcbtn,novechtxt;
    private ImageView fcimage,rcimage,insureimage;
    private Session session;
    private  AlertDialog.Builder builder;
    private String Url="assignVehicle";
    private String fcImg,rcImg,insureImg;
    private Button button;
    private RelativeLayout overalllayout;
    ImageView image;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allotted_vehicle);
        session = new Session(this);
        vechno=findViewById(R.id.vechno);
        vectype=findViewById(R.id.vechtype);
        fcimage=findViewById(R.id.fcimg);
        rcimage=findViewById(R.id.rcimg);
        insureimage=findViewById(R.id.insurannce);
        insurebtn=findViewById(R.id.insuredownbtn);
        fcbtn=findViewById(R.id.fcdownbtn);
        rcbtn=findViewById(R.id.rcdownbtn);
        overalllayout=findViewById(R.id.allotrelative);
        novechtxt=findViewById(R.id.novech);
        JSONObject reqobj=new JSONObject();
        try {
            reqobj.put("driverId",String.valueOf(session.getUserId()));
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        getInitialValues(String.valueOf(reqobj));
        insurebtn.setOnClickListener(this);
        fcbtn.setOnClickListener(this);
        rcbtn.setOnClickListener(this);
    }
    private void getInitialValues(String response) {
        final LoadingDialog loadingDialog=new LoadingDialog(this);
        try {
            loadingDialog.startLoadingDialog();
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  StaticInfo.allotedvehicle_Url+Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        loadingDialog.dismissDialog();
                        Log.d("strrrr",response);
                        JSONObject obj = new JSONObject(response);
                        if(obj.getString("error")=="true"){
                            overalllayout.setVisibility(View.GONE);
                            novechtxt.setVisibility(View.VISIBLE);
                        }
                        else{
                            overalllayout.setVisibility(View.VISIBLE);
                            novechtxt.setVisibility(View.GONE);
                            JSONArray dataarray=obj.getJSONArray("data");
                            Log.d("dataArray",""+dataarray.length());
                            for (int i=0;i<dataarray.length();i++) {
                                JSONObject dataobj = dataarray.getJSONObject(i);
                                vechno.setText(dataobj.getString("reg_no"));
                                vectype.setText(dataobj.getString("vehicle_type"));
                                if (!dataobj.getString("fc_img").equals("")) {
                                    Glide.with(AllottedVehicle.this)
                                            .load(StaticInfo.LocalImgPath + dataobj.getString("fc_img"))
                                            .into(fcimage);
                                    fcImg = dataobj.getString("fc_img");
                                }
                                if (!dataobj.getString("reg_certificate_img").equals("")) {
                                    Glide.with(AllottedVehicle.this)
                                            .load(StaticInfo.LocalImgPath + dataobj.getString("reg_certificate_img"))
                                            .into(rcimage);
                                    rcImg = dataobj.getString("reg_certificate_img");
                                }
                                if (!dataobj.getString("insurance_img").equals("")) {
                                    Glide.with(AllottedVehicle.this)
                                            .load(StaticInfo.LocalImgPath+dataobj.getString("insurance_img"))
                                            .into(insureimage);
                                    insureImg = dataobj.getString("insurance_img");
                                }
                            }
                            Log.d("strrrr",response);
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if(error instanceof NoConnectionError){
                        ConnectivityManager cm = (ConnectivityManager)getContext()
                                .getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetwork = null;
                        if (cm != null) {
                            activeNetwork = cm.getActiveNetworkInfo();
                        }
                        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                            Toast.makeText(AllottedVehicle.this, "Server is not connected to internet.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(AllottedVehicle.this, "Your device is not connected to internet.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    else if (error instanceof NetworkError || error.getCause() instanceof ConnectException
                            || (error.getCause().getMessage() != null
                            && error.getCause().getMessage().contains("connection"))){
                        Toast.makeText(AllottedVehicle.this, "Your device is not connected to internet.",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (error.getCause() instanceof MalformedURLException){
                        Toast.makeText(AllottedVehicle.this, "Bad Request.", Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof ParseError || error.getCause() instanceof IllegalStateException
                            || error.getCause() instanceof JSONException
                            || error.getCause() instanceof XmlPullParserException){
                        Toast.makeText(AllottedVehicle.this, "Parse Error (because of invalid json or xml).",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (error.getCause() instanceof OutOfMemoryError){
                        Toast.makeText(AllottedVehicle.this, "Out Of Memory Error.", Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof AuthFailureError){
                        Toast.makeText(AllottedVehicle.this, "server couldn't find the authenticated request.",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
                        Toast.makeText(AllottedVehicle.this, "Server is not responding.", Toast.LENGTH_SHORT).show();
                    }
                    else if (error instanceof TimeoutError || error.getCause() instanceof SocketTimeoutException
                            || error.getCause() instanceof ConnectTimeoutException
                            || error.getCause() instanceof SocketException
                            || (error.getCause().getMessage() != null
                            && error.getCause().getMessage().contains("Connection timed out"))) {
                        Toast.makeText(AllottedVehicle.this, "Connection timeout error",
                                Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(AllottedVehicle.this, "An unknown error occurred.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            Log.d("stringRequest",""+stringRequest);
            requestQueue.add(stringRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==insurebtn){
            Log.d("click insure","insured");
            new DownloadsImage().execute(StaticInfo.LocalImgPath+insureImg);
        }
        else if (view==fcbtn){
            new DownloadsImage().execute(StaticInfo.LocalImgPath+fcImg);
        }
        else if (view==rcbtn){
            new DownloadsImage().execute(StaticInfo.LocalImgPath+rcImg);
        }

    }

    private class DownloadsImage extends AsyncTask<String, Void,Void>{
        @Override
        protected Void doInBackground(String... strings) {
            URL url = null;
            try {
                url = new URL(strings[0]);
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            Bitmap bm = null;
            try {
                bm =    BitmapFactory.decodeStream(url.openConnection().getInputStream());
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            //Create Path to save Image
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES+ "/vtl"); //Creates app specific folder
            if(!path.exists()) {
                path.mkdirs();
            }
            File imageFile = new File(path, String.valueOf(System.currentTimeMillis())+".png"); // Imagename.png
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(imageFile);
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            try{
                bm.compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
                out.flush();
                out.close();
                // Tell the media scanner about the new file so that it is
                // immediately available to the user.
                MediaScannerConnection.scanFile(AllottedVehicle.this,new String[] { imageFile.getAbsolutePath() }, null,new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                         Log.i("ExternalStorage", "Scanned " + path + ":");
                    }
                });
            }
            catch(Exception e) {
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(AllottedVehicle.this,"Downloaded Successfully",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(AllottedVehicle.this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}