package com.lia.vtl.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context _context;
    private static final String IS_LOGIN = "IsLoggedIn";

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setusename(String usename) {
        prefs.edit().putString("usename", usename).commit();
    }

    public String getarticle() {
        String article = prefs.getString("article","");
        return article;
    }
    public void setarticle(String article) {
        prefs.edit().putString("article", article).commit();
    }

    public String getusename() {
        String usename = prefs.getString("usename","");
        return usename;
    }
    public void setUserId(int userId) {
        prefs.edit().putInt("userId", userId).commit();
    }

    public int getUserId() {
        int userId = prefs.getInt("userId",0);
        return userId;
    }
    public void setlang(String Lang) {
        prefs.edit().putString("Lang", Lang).commit();
    }

    public String getlang() {
        String Lang = prefs.getString("Lang","ta");
        return Lang;
    }
  }

