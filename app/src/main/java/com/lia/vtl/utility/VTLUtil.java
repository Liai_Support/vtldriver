package com.lia.vtl.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class VTLUtil {
    @SuppressLint("StaticFieldLeak")
    private static final VTLUtil ourInstance = new VTLUtil();
    public static String currentPhotoPath;
    public static Context getContext() {
        return context;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public static Context context;
    public static Pattern aadhaarPattern = Pattern.compile("^[2-9]{1}[0-9]{11}$");
    public static Pattern licencePattern = Pattern.compile("^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$");
    public static Pattern panCardPattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
    public static Pattern voterIdPattern = Pattern.compile("[A-Z]{3}[0-9]{7}");
    public String[] months = new String[]{"Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static AmazonS3 s3Client;
    public static String bucket = "vtl-app";
    public static TransferUtility transferUtility;
    List<String> listing;
    public static int percentage;
    public static ACProgressFlower progressFlower;
    private VTLUtil() {
    }

    public static VTLUtil getInstance() {
        return ourInstance;
    }

    public static void s3credentialsProvider(){
        // Initialize the AWS Credential
       /* CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider =
                new CognitoCachingCredentialsProvider(
                        getContext(),
                        "us-east-1:4f62338b-6520-4786-8530-b62c1146564f", // Identity pool ID
                        Regions.US_EAST_1 // Region*/
//                );
// Initialize the Amazon Cognito credentials provider
        CognitoCachingCredentialsProvider cognitoCachingCredentialsProvider = new CognitoCachingCredentialsProvider(
                getContext(),
                "us-east-1:14d94898-d87d-4c7f-b6a4-32f6141f0ffc", // Identity pool ID
                Regions.US_EAST_1 // Region
        );
        createAmazonS3Client(cognitoCachingCredentialsProvider);
    }
    /**
     *  Create a AmazonS3Client constructor and pass the credentialsProvider.
     * @param credentialsProvider
     */
    public static void createAmazonS3Client(CognitoCachingCredentialsProvider
                                             credentialsProvider){
        // Create an S3 client
        s3Client = new AmazonS3Client(credentialsProvider);
        // Set the region of your S3 bucket
        s3Client.setRegion(Region.getRegion(Regions.US_EAST_1));
    }
    public static void setTransferUtility(){
        transferUtility = new TransferUtility(s3Client, getContext().getApplicationContext());
    }

    public static void progressFlowerDismiss(ACProgressFlower progressFlower) {
        if (progressFlower.isShowing()) {
            progressFlower.dismiss();
        }
    }

    public static void loadingDialog() {
        progressFlower = new ACProgressFlower.Builder(getContext().getApplicationContext())
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .text("Please Wait..")
                .fadeColor(Color.DKGRAY).build();
        progressFlower.show();
    }

    /**
     * This is listener method of the TransferObserver
     * Within this listener method, we get status of uploading and downloading file,
     * to display percentage of the part of file to be uploaded or downloaded to S3
     * It displays an error, when there is a problem in  uploading or downloading file to or from S3.
     * @param transferObserver
     */

    public static void transferObserverListener(TransferObserver transferObserver){
        transferObserver.setTransferListener(new TransferListener(){
            @Override
            public void onStateChanged(int id, TransferState state) {
                Toast.makeText(getContext(), "State Change" + state, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                percentage = (int) (bytesCurrent/bytesTotal * 100);
                Toast.makeText(getContext(), "Progress in %"
                        + percentage, Toast.LENGTH_SHORT).show();
                Log.d("perc",""+percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error","error");
            }
        });
    }

    /*
Bitmap Convert Base64 String format
 */
    public String bitMapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }
}