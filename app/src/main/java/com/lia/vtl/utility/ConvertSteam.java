package com.lia.vtl.utility;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ConvertSteam {

    public  String ConvertSteamToString(InputStream inputStream) {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        catch (Exception e) {
           Log.e("", "Error in ConvertStreamToString", e);
        }
        finally {
            try {
                inputStream.close();

            } catch (Exception e) {
                Log.e("", "Error in ConvertStreamToString", e);
            }
        }
        return stringBuilder.toString();
    }
}
