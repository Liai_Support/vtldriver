package com.lia.vtl.utility;

import android.util.Base64;
import android.util.Log;

import com.lia.vtl.bo.Driver;
import com.lia.vtl.bo.Article;
//import com.ua.uaexpert.bo.BookingDetails;
//import com.ua.uaexpert.bo.CreditsDetails;
//import com.ua.uaexpert.bo.DebitDetails;
//import com.ua.uaexpert.bo.Earnings;
//import com.ua.uaexpert.bo.Rate;
//import com.ua.uaexpert.bo.Technician;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class StaticInfo {
   //public static String MAIN_URL="http://192.168.0.104/uaservice/tech.php?action=";
   //  public static String MAIN_URL="http://env-6004661.cloud.cms500.com/uaservice/tech.php?action=";
   //public static String MAIN_URL="http://universalairconditioner.com/uaservice/tech.php?action=";
    public static String MAIN_URL="https://vtlpl.com/new_app/service/tech.php?action=";
    public static String TEST_URL="https://vtlpl.com/new_app/service/tech.php?action=";
    public static String NEW_URL="https://www.vtlpl.com/vtlapi/driver/";
    public static String allotedvehicle_Url="https://www.vtlpl.com/vtlapi/transporter/";
    public static String LocalImgPath="https://www.vtlpl.com/new_app/upload/";
    public static String DRIVER_STATUS="driverOnlineStatus";
    public static String DRIVER_UPDATE="updateDriver";
    public static String imgStrBase="";
    public static String imgPathStr="https://vtl-app.s3.amazonaws.com/";
//    public static String RATE_URL="http://192.168.1.31/uaservice/rate.txt";
    public static String otpvalue="";
    public static boolean credit=false;
    public static ArrayList<Article> listArticle=new ArrayList<Article>();
    public static Article articleList;
    public static Driver driver=new Driver();
    public static Article article=new Article();
    public static int userId=9;
    public static String mobNo="";

    public static boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static String getTechDetailsUrl(int techId ){
        String finalUrl=MAIN_URL+"techDetails";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getRateUrl(int catId ){
        String finalUrl=MAIN_URL+"ratecard";
        finalUrl+="&catId="+catId;
        return finalUrl;
    }

    public static String getLoginUrl(String mobile, String password){
        String finalUrl=MAIN_URL+"driverlogin";
        finalUrl+="&mobile="+mobile;
        finalUrl+="&password="+password;
        return finalUrl;
    }

    public static String checkMobileNumber(String mobileNo){
        String finalURL=MAIN_URL+"drivermobilecheck";
        finalURL+="&mobile="+mobileNo;
        return finalURL;
    }

    public static String getcheckMobileUrl(String mobile){
        String finalUrl=MAIN_URL+"drivermobilecheck";
        finalUrl+="&mobile="+mobile;
        return finalUrl;
    }

    public static String getForgetUrl(String mobile){
        String finalUrl=MAIN_URL+"forget";
        finalUrl+="&mobile="+mobile;
        return finalUrl;
    }

    public static String getchangepswdUrl(int id,String cpswd,String pswd){
        String finalUrl=MAIN_URL+"changePassword";
        finalUrl+="&id="+id;
        finalUrl+="&cpswd="+cpswd;
        finalUrl+="&pswd="+pswd;
        Log.d("jgytydtersfc",""+finalUrl);
        return finalUrl;
    }

    public static String getDriverDetailsUrl(int id){
        String finalUrl=MAIN_URL+"driverdetails";
        finalUrl+="&id="+id;
        return finalUrl;
    }

    public static String getChangeUrl(String pass, int id){
        String finalUrl=MAIN_URL+"change";
        finalUrl+="&techId="+id;
        finalUrl+="&password="+pass;
        return finalUrl;
    }


    public static String getRegisterUrl(String mobileStr, String passwordStr, String statusStr, String imei){
        String finalUrl = MAIN_URL + "register";
        try {
            finalUrl += "&mobile=" + mobileStr;
            finalUrl += "&password=" + passwordStr;
            finalUrl += "&status=" + statusStr;
            finalUrl += "&imei=" + imei;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return finalUrl;
    }
//    $id,$uname,$user_img,$dob,$gender,$marital_status,$address,$city,$state,$country,$aadhaar_no,$aadhaar_img,$pancard,
//    $license_no,$licence_front_img,$licence_back_img,$license_expiry_date,$batch,$batch_expiry,$driving_experience,$vehicle_handling,
//    $emergency_contact_no,$insurance,$employee_type,$reference_person_name,$reference_contact_no,$bank_ac_no,$passbook_img,$advance_payment,
//    $salary_cycle,$split_payment,$status,$pincode
    public static String getUpdateRegisterUrl(int id, String uname, String userImg, String dob, String gender,String maritalStatus,String address,
      String city,String state,String country,String adhar,String aadhaarImg,String pancard,String licenseNo,
      String licenceFrontImg,String licenceBackImg,String licenseExpiryDate,String batch,String batchExpiry,String drivingExperience,String vehicleHandling,
      String emerContNo,String insurance,String employeeType,String referPersonName,String referContactNo,String bankAcNo,String passbookImg,
      String advancePayment,String salaryCycle,String splitPayment,String status,String pincode){
        String finalUrl = MAIN_URL + "updateDriver";
        try {
            byte[] data2 = uname.getBytes("UTF-8");
            String enName = Base64.encodeToString(data2, Base64.DEFAULT);
            finalUrl += "&id=" + id;
            finalUrl += "&uname=" + enName.replace("\n","");
            finalUrl += "&userImg=" + URLEncoder.encode(userImg,"UTF8");
            finalUrl += "&dob=" + dob;
            finalUrl += "&gender=" + gender;
            finalUrl += "&maritalStatus=" + maritalStatus;
            finalUrl += "&address=" + address;
            finalUrl += "&city=" + city;
            finalUrl += "&state=" + state;
            finalUrl += "&country=" + country;
            finalUrl += "&adhar=" + adhar;
            finalUrl += "&aadhaarImg=" + URLEncoder.encode(aadhaarImg,"UTF8");
            finalUrl += "&pancard=" + pancard;
            finalUrl += "&licenseNo=" + licenseNo;
            finalUrl += "&licenceFrontImg=" + URLEncoder.encode(licenceFrontImg,"UTF8");
            finalUrl += "&licenceBackImg=" +URLEncoder.encode(licenceBackImg,"UTF8") ;
            finalUrl += "&licenseExpiryDate=" + licenseExpiryDate;
            finalUrl += "&batch=" + batch;
            finalUrl += "&batchExpiry=" + batchExpiry;
            finalUrl += "&drivingExperience=" + drivingExperience;
            finalUrl += "&vehicleHandling=" + vehicleHandling;
            finalUrl += "&emerContNo=" + emerContNo;
            finalUrl += "&insurance=" + insurance;
            finalUrl += "&employeeType=" + employeeType;
            finalUrl += "&referPersonName=" + referPersonName;
            finalUrl += "&referContactNo=" + referContactNo;
            finalUrl += "&bankAcNo=" + bankAcNo;
            finalUrl += "&passbookImg=" + passbookImg;
            finalUrl += "&advancePayment=" + advancePayment;
            finalUrl += "&salaryCycle=" + salaryCycle;
            finalUrl += "&splitPayment=" + splitPayment;
            finalUrl += "&status=" + status;
            finalUrl += "&pincode=" + pincode;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return finalUrl;
    }

    public static String getAlertUrl(int techId){
        String finalUrl=MAIN_URL+"alert";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getArticleUrl(){
        String finalUrl=MAIN_URL+"listAcArticles";
        return finalUrl;
    }

    public static String getArticleByIdUrl(int id){
        String finalUrl=MAIN_URL+"articleById";
        finalUrl+="&id="+id;
        return finalUrl;
    }

    public static String getListLeadsUrl(int techId){
        String finalUrl=MAIN_URL+"listleads";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getCredits(int techId){
        String finalUrl=MAIN_URL+"credits";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getEarnings(int techId){
        String finalUrl=MAIN_URL+"earnings";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getDebit(int techId){
        String finalUrl=MAIN_URL+"debit";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getAlertStatus(int techId, int serviceId, String status){
        String finalUrl=MAIN_URL+"alertstatus";
        finalUrl+="&techId="+techId;
        finalUrl+="&serviceId="+serviceId;
        finalUrl+="&status="+status;
        return finalUrl;
    }

    public static String getListServiceUrl(int techId){
        String finalUrl=MAIN_URL+"listservice";
        finalUrl+="&techId="+techId;
        return finalUrl;
    }

    public static String getUpdateStatus(int serviceId, String status, int techId, String comment){
        byte[] data = new byte[0];
        try {
            data = comment.getBytes("UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String enCat = Base64.encodeToString(data, Base64.DEFAULT);
        String finalUrl=MAIN_URL+"updateStatus";
        finalUrl+="&serviceId="+serviceId;
        finalUrl+="&status="+status;
        finalUrl+="&techId="+techId;
        finalUrl+="&comment="+enCat;
        return finalUrl.replace(" ","").replace("\n","");
    }

    public static String getEndJob(int serviceId, String status, int techId, String items, String total){
        byte[] data2 = new byte[10000];
        String finalUrl=MAIN_URL+"endJob";
        try {
            data2 = items.getBytes("UTF-8");
        String itemNames = Base64.encodeToString(data2, Base64.DEFAULT);
        finalUrl+="&serviceId="+serviceId;
        finalUrl+="&status="+status;
        finalUrl+="&techId="+techId;
        finalUrl+="&items="+itemNames.replace(" ","");
        finalUrl+="&total="+total;
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return finalUrl.replace(" ","").replace("\n","");
    }

    public static String getEndJobItem(int itemId, String itemName, String itemQty, String itemPrice, String totalAmount, int serviceId){
        byte[] data2 = new byte[10000];
        String finalUrl=MAIN_URL+"invoiceItem";
        try {
            //data2 = itemName.getBytes("UTF-8");
            //String itemNames = Base64.encodeToString(data2, Base64.DEFAULT);
            finalUrl+="&itemId="+serviceId;
            finalUrl+="&itemName="+itemName;
            finalUrl+="&itemQty="+itemQty;
            finalUrl+="&itemPrice="+itemPrice;
            finalUrl+="&totalAmount="+totalAmount;
            finalUrl+="&serviceId="+serviceId;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return finalUrl.replace(" ","").replace("\n","");
        //return finalUrl;
    }

    public static String getleadDetails(int serviceId){
        String finalUrl=MAIN_URL+"leadDetails";
        finalUrl+="&serviceId="+serviceId;
        return finalUrl;
    }

    public static boolean validEmail(String email) {
        // editing to make requirements listed
        // return email.matches("[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}");
        return email.matches("[A-Z0-9._%+-][A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{3}");
    }

    public static boolean mobile(String val){
        try{
            Long.parseLong(val);
            if(val.length()==10){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    /*
     Forgot Password
     */
    public static String getForgotPWDUrl(int currentUserID, String password){
        String finalUrl = MAIN_URL + "updateDriverPassword";
        try {
            finalUrl += "&userid=" + currentUserID;
            finalUrl += "&password=" + password;
           }catch (Exception e){
            e.printStackTrace();
        }
        return finalUrl;
    }
}
