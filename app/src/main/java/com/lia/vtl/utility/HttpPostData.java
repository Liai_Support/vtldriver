package com.lia.vtl.utility;

import android.util.Log;



import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public  class HttpPostData {
    private String errorMessage = null;
    public InputStream ByPostMethod(String ServerURL,String PostParam) {
       InputStream inputStream = null;
        try {
            Log.i("HttpPostData", "ByPostMethod: 1");
            URL url = new URL(ServerURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setReadTimeout(0);
            httpURLConnection.setConnectTimeout(0);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8" );
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            Log.i("HttpPostData", "ByPostMethod: 2");
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.writeBytes(PostParam);
            dataOutputStream.flush();
            dataOutputStream.close();
            int response = httpURLConnection.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                Log.i("HttpPostData", String.valueOf(response));
                inputStream = httpURLConnection.getInputStream();
            }
            else if (response == HttpURLConnection.HTTP_BAD_REQUEST) {
                Log.i("HttpPostData", String.valueOf(response));
                inputStream = httpURLConnection.getErrorStream();
            }
            else {
                errorMessage = httpURLConnection.getResponseMessage();
                Log.i("HttpPostData", errorMessage);
                setErrorMessage(errorMessage);
            }
        }
        catch (Exception e) {
            Log.e("PostData", "Error in PostData", e);
            errorMessage = e.getMessage();
            setErrorMessage(errorMessage);
        }
        Log.i("HttpPostData", "ByPostMethod: 5");
        return inputStream;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage(){
        return errorMessage;
    }
}




