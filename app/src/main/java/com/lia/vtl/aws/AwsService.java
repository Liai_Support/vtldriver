package com.lia.vtl.aws;

import android.annotation.SuppressLint;
import android.content.Context;
import android.preference.PreferenceManager;

import com.lia.vtl.utility.VTLUtil;

public class AwsService {
    @SuppressLint("StaticFieldLeak")
    private static final AwsService ourInstance = new AwsService();

    private String currentPhotoPath;

    public static Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static Context context;

    private AwsService() { }

    public static AwsService getInstance() {
        return ourInstance;
    }
}
