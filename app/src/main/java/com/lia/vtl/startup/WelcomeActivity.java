package com.lia.vtl.startup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.os.Build;
import android.os.Bundle;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.google.firebase.iid.FirebaseInstanceId;
import com.lia.vtl.bo.TransactionBo;
import com.lia.vtl.service.AlertService;
import com.lia.vtl.view.DashboardActivity;
//import com.lia.vtl.view.LoadingDialog;
import com.lia.vtl.view.LoginActivity;
import com.lia.vtl.R;
import com.lia.vtl.bo.Driver;
import com.lia.vtl.service.ArticleService;
import com.lia.vtl.utility.PrefManager;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.lia.vtl.utility.VTLUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class WelcomeActivity extends AppCompatActivity {
private ViewPager viewPager;
private MyViewPagerAdapter myViewPagerAdapter;
private LinearLayout dotsLayout;
private TextView[] dots;
private int[] layouts;
private Button btnSkip, btnNext;
private PrefManager prefManager;
private Session session;
private AlertService alertService;
private ArticleService articleService;
private VTLUtil __UTIL = VTLUtil.getInstance();
private String Url="https://vtlpl.com/new_app/service/tech.php?action=updateDriverToken";
public static final String MyPREFERENCES = "MyPrefs" ;

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(this);
        session = new Session(this);
        alertService = new AlertService(this);
        articleService = new ArticleService(this);
        __UTIL.setContext(this);
        if (!prefManager.isFirstTimeLaunch()) {
        launchHomeScreen();
        finish();
        }
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_welcome);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);
        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
        R.layout.welcome_slide1,
        R.layout.welcome_slide2
       };
        // adding bottom dots
        addBottomDots(0);
        // making notification bar transparent
        changeStatusBarColor();
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        btnSkip.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        launchHomeScreen();
        }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        // checking for last page
        // if last page home screen will be launched
        int current = getItem(+1);
        if (current < layouts.length) {
        // move to next screen
        viewPager.setCurrentItem(current);
        } else {
        launchHomeScreen();
        }
        }
        });
        }

private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];
        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
        dots[i] = new TextView(this);
        dots[i].setText(Html.fromHtml("&#8226;"));
        dots[i].setTextSize(35);
        dots[i].setTextColor(colorsInactive[currentPage]);
        dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
        dots[currentPage].setTextColor(colorsActive[currentPage]);
        }

private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
        }

private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        try {
        if (session.getusename() != null && session.getusename().length() > 0) {
            ObjectMapper mapper = new ObjectMapper();
            ArrayList<Driver> driverlist = mapper.readValue(session.getusename(), new TypeReference<ArrayList<Driver>>(){});
            StaticInfo.driver = driverlist.get(0);
            StaticInfo.userId= StaticInfo.driver.getId();
            Toast.makeText(this,StaticInfo.driver.getStatus(),Toast.LENGTH_LONG);
//            if (StaticInfo.driver.getStatus() == "A" || StaticInfo.driver.getStatus().equals("A") ) {
//                Toast.makeText(getBaseContext(),StaticInfo.driver.getStatus(),Toast.LENGTH_LONG);
//                alertService.createNotification(getString(R.string.notify_title), getString(R.string.notify_message_first)+ StaticInfo.driver.getUname() + getString(R.string.notify_message));
//            }
            startActivity(new Intent(WelcomeActivity.this, DashboardActivity.class));
            String token = FirebaseInstanceId.getInstance().getToken();
            JSONObject obj=new JSONObject();
            obj.put("driver_id",StaticInfo.userId);
            obj.put("token",token);
            getInitialValues(""+obj);
        }
        else {
            startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
        }
        }
        catch(Exception e){
            Log.d("Error",e.getMessage());
        }
        }
    private void getInitialValues(String response) {
        try {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            JSONObject jsonBody = new JSONObject(response);
            final String requestcartBody = jsonBody.toString();
            Log.d("requesttoken",""+requestcartBody);
            StringRequest stringRequest = new StringRequest(Request.Method.POST,  Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("requesttoken",""+response);
                 if (response=="success"){
                 }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }
                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }
                }
            };
            requestQueue.add(stringRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    //	viewpager change listener
        ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
@Override
public void onPageSelected(int position) {
        addBottomDots(position);
        // changing the next button text 'NEXT' / 'GOT IT'
        if (position == layouts.length - 1) {
        // last page. make button text to GOT IT
        btnNext.setText(getString(R.string.start));
        btnSkip.setVisibility(View.GONE);
        }
        else {
        // still pages are left
        btnNext.setText(getString(R.string.next));
        btnSkip.setVisibility(View.VISIBLE);
        }
        }

@Override
public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

@Override
public void onPageScrollStateChanged(int arg0) {
        }
        };

/**
 * Making notification bar transparent
 */
private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);
        }
        }

/**
 * View pager adapter
 */
public class MyViewPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    public MyViewPagerAdapter() {
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(layouts[position], container, false);
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}




}
