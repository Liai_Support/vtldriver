package com.lia.vtl.startup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.lia.vtl.view.DashboardActivity;
import com.lia.vtl.R;

public class ThanksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over 7secs
                Intent i = new Intent(ThanksActivity.this, DashboardActivity.class);
                startActivity(i);
                finish();
            }
        }, 5000);
    }
}