package com.lia.vtl.bo;

public class Driver {
    private int id;
    private String uname;
    private String upassword;
    private String userImg;
    private String dob;
    private String gender;
    private String maritalStatus;
    private String contactNo;
    private String address;
    private String city;
    private String state;
    private String country;
    private String adhar;
    private String aadhaarImg;
    private String pancard;
    private String licenseNo;
    private String licenceFrontImg;
    private String licenceBackImg;
    private String licenseExpiryDate;
    private String batch;
    private String batchExpiry;
    private String drivingExperience;
    private String vehicleHandling;
    private String emerContNo;
    private String insurance;
    private String employeeType;
    private String referPersonName;
    private String referContactNo;
    private String bankAcNo;
    private String passbookImg;
    private String advancePayment;
    private String salaryCycle;
    private String splitPayment;
    private String status;
    private String pincode;
    private String oStatus;
    private String token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpassword() {
        return upassword;
    }

    public void setUpassword(String upassword) {
        this.upassword = upassword;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAdhar() {
        return adhar;
    }

    public void setAdhar(String adhar) {
        this.adhar = adhar;
    }

    public String getAadhaarImg() {
        return aadhaarImg;
    }

    public void setAadhaarImg(String aadhaarImg) {
        this.aadhaarImg = aadhaarImg;
    }

    public String getPancard() {
        return pancard;
    }

    public void setPancard(String pancard) {
        this.pancard = pancard;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getLicenceFrontImg() {
        return licenceFrontImg;
    }

    public void setLicenceFrontImg(String licenceFrontImg) {
        this.licenceFrontImg = licenceFrontImg;
    }

    public String getLicenceBackImg() {
        return licenceBackImg;
    }

    public void setLicenceBackImg(String licenceBackImg) {
        this.licenceBackImg = licenceBackImg;
    }

    public String getLicenseExpiryDate() {
        return licenseExpiryDate;
    }

    public void setLicenseExpiryDate(String licenseExpiryDate) {
        this.licenseExpiryDate = licenseExpiryDate;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getBatchExpiry() {
        return batchExpiry;
    }

    public void setBatchExpiry(String batchExpiry) {
        this.batchExpiry = batchExpiry;
    }

    public String getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(String drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    public String getVehicleHandling() {
        return vehicleHandling;
    }

    public void setVehicleHandling(String vehicleHandling) {
        this.vehicleHandling = vehicleHandling;
    }

    public String getEmerContNo() {
        return emerContNo;
    }

    public void setEmerContNo(String emerContNo) {
        this.emerContNo = emerContNo;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public String getReferPersonName() {
        return referPersonName;
    }

    public void setReferPersonName(String referPersonName) {
        this.referPersonName = referPersonName;
    }

    public String getReferContactNo() {
        return referContactNo;
    }

    public void setReferContactNo(String referContactNo) {
        this.referContactNo = referContactNo;
    }

    public String getBankAcNo() {
        return bankAcNo;
    }

    public void setBankAcNo(String bankAcNo) {
        this.bankAcNo = bankAcNo;
    }

    public String getPassbookImg() {
        return passbookImg;
    }

    public void setPassbookImg(String passbookImg) {
        this.passbookImg = passbookImg;
    }

    public String getAdvancePayment() {
        return advancePayment;
    }

    public void setAdvancePayment(String advancePayment) {
        this.advancePayment = advancePayment;
    }

    public String getSalaryCycle() {
        return salaryCycle;
    }

    public void setSalaryCycle(String salaryCycle) {
        this.salaryCycle = salaryCycle;
    }

    public String getSplitPayment() {
        return splitPayment;
    }

    public void setSplitPayment(String splitPayment) {
        this.splitPayment = splitPayment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getoStatus() {
        return oStatus;
    }

    public void setoStatus(String oStatus) {
        this.oStatus = oStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}