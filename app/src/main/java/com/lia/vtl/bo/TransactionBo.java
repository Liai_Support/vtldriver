package com.lia.vtl.bo;

public class TransactionBo {
    private int tripId;
    private String tripDate;
    private String tripMovDate;
    private String tripFrom;
    private String tripTo;
    private String tripPayType;
    private String tripTrip;
    private String tripStatus;
    private String tripSal;
    private String tripPay;
    private String tripBal;
    private String tripToll;
    private String tripRepair;
    private String tripMiscel;
    private String tripTotExp;
    private String tripTotbal;

    public TransactionBo(int tripId,String tripDate,String tripMovDate,String tripFrom,String tripTo,String tripPayType,String tripTrip,String tripStatus,String tripSal,String tripPay
    ,String tripBal,String tripToll,String tripRepair,String tripMiscel,String tripTotExp,String tripTotbal){
        this.tripId=tripId;
        this.tripDate=tripDate;
        this.tripMovDate=tripMovDate;
        this.tripFrom=tripFrom;
        this.tripTo=tripTo;
        this.tripPayType=tripPayType;
        this.tripTrip=tripTrip;
        this.tripStatus=tripStatus;
        this.tripSal=tripSal;
        this.tripPay=tripPay;
        this.tripBal=tripBal;
        this.tripToll=tripToll;
        this.tripRepair=tripRepair;
        this.tripMiscel=tripMiscel;
        this.tripTotExp=tripTotExp;
        this.tripTotbal=tripTotbal;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripMovDate() {
        return tripMovDate;
    }

    public void setTripMovDate(String tripMovDate) {
        this.tripMovDate = tripMovDate;
    }

    public String getTripFrom() {
        return tripFrom;
    }

    public void setTripFrom(String tripFrom) {
        this.tripFrom = tripFrom;
    }

    public String getTripTo() {
        return tripTo;
    }

    public void setTripTo(String tripTo) {
        this.tripTo = tripTo;
    }

    public String getTripPayType() {
        return tripPayType;
    }

    public void setTripPayType(String tripPayType) {
        this.tripPayType = tripPayType;
    }

    public String getTripTrip() {
        return tripTrip;
    }

    public void setTripTrip(String tripTrip) {
        this.tripTrip = tripTrip;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTripSal() {
        return tripSal;
    }

    public void setTripSal(String tripSal) {
        this.tripSal = tripSal;
    }

    public String getTripPay() {
        return tripPay;
    }

    public void setTripPay(String tripPay) {
        this.tripPay = tripPay;
    }

    public String getTripBal() {
        return tripBal;
    }

    public void setTripBal(String tripBal) {
        this.tripBal = tripBal;
    }

    public String getTripToll() {
        return tripToll;
    }

    public void setTripToll(String tripToll) {
        this.tripToll = tripToll;
    }

    public String getTripRepair() {
        return tripRepair;
    }

    public void setTripRepair(String tripRepair) {
        this.tripRepair = tripRepair;
    }

    public String getTripMiscel() {
        return tripMiscel;
    }

    public void setTripMiscel(String tripMiscel) {
        this.tripMiscel = tripMiscel;
    }

    public String getTripTotExp() {
        return tripTotExp;
    }

    public void setTripTotExp(String tripTotExp) {
        this.tripTotExp = tripTotExp;
    }

    public String getTripTotbal() {
        return tripTotbal;
    }

    public void setTripTotbal(String tripTotbal) {
        this.tripTotbal = tripTotbal;
    }
}
