package com.lia.vtl.bo;

public class MultiImg {
    private String imgName;

    public  MultiImg(String imgName){
        this.imgName=imgName;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }
}
