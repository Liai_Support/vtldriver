package com.lia.vtl.bo;

public class Expenses {
    private int expId;
    private String exptollTxt;
    private String exptollImg;
    private String exprepairCharges;
    private String exprepImage;
    private String expmiscCharges;
    private String expmiscDet;
    public Expenses(int expId,String exptollTxt,String exptollImg,String exprepairCharges,String exprepImage,String expmiscCharges,String expmiscDet){
        this.expId=expId;
        this.exptollTxt=exptollTxt;
        this.exptollImg=exptollImg;
        this.exprepairCharges=exprepairCharges;
        this.exprepImage=exprepImage;
        this.expmiscCharges=expmiscCharges;
        this.expmiscDet=expmiscDet;
    }

    public int getExpId() {
        return expId;
    }

    public void setExpId(int expId) {
        this.expId = expId;
    }

    public String getExptollTxt() {
        return exptollTxt;
    }

    public void setExptollTxt(String exptollTxt) {
        this.exptollTxt = exptollTxt;
    }

    public String getExptollImg() {
        return exptollImg;
    }

    public void setExptollImg(String exptollImg) {
        this.exptollImg = exptollImg;
    }

    public String getExprepairCharges() {
        return exprepairCharges;
    }

    public void setExprepairCharges(String exprepairCharges) {
        this.exprepairCharges = exprepairCharges;
    }

    public String getExprepImage() {
        return exprepImage;
    }

    public void setExprepImage(String exprepImage) {
        this.exprepImage = exprepImage;
    }

    public String getExpmiscCharges() {
        return expmiscCharges;
    }

    public void setExpmiscCharges(String expmiscCharges) {
        this.expmiscCharges = expmiscCharges;
    }

    public String getExpmiscDet() {
        return expmiscDet;
    }

    public void setExpmiscDet(String expmiscDet) {
        this.expmiscDet = expmiscDet;
    }
}
